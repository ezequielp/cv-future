<!DOCTYPE html>
<!-- saved from url=(0036)https://certy.px-lab.com/html/demo1/ -->
<html lang="en" class="crt crt-side-box-on crt-nav-on crt-nav-type2 crt-main-nav-on crt-sidebar-on crt-layers-1 desktop crt-desktop" style="overflow: visible;"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css">.dismissButton{background-color:#fff;border:1px solid #dadce0;color:#1a73e8;border-radius:4px;font-family:Roboto,sans-serif;font-size:14px;height:36px;cursor:pointer;padding:0 24px}.dismissButton:hover{background-color:rgba(66,133,244,0.04);border:1px solid #d2e3fc}.dismissButton:focus{background-color:rgba(66,133,244,0.12);border:1px solid #d2e3fc;outline:0}.dismissButton:hover:focus{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd}.dismissButton:active{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd;box-shadow:0 1px 2px 0 rgba(60,64,67,0.3),0 1px 3px 1px rgba(60,64,67,0.15)}.dismissButton:disabled{background-color:#fff;border:1px solid #f1f3f4;color:#3c4043}
</style><style type="text/css">.gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{font-weight:400}
</style><style type="text/css">.gm-control-active>img{box-sizing:content-box;display:none;left:50%;pointer-events:none;position:absolute;top:50%;transform:translate(-50%,-50%)}.gm-control-active>img:nth-child(1){display:block}.gm-control-active:hover>img:nth-child(1),.gm-control-active:active>img:nth-child(1){display:none}.gm-control-active:hover>img:nth-child(2),.gm-control-active:active>img:nth-child(3){display:block}
</style><link type="text/css" rel="stylesheet" href="./Certy_files/css"><style type="text/css">.gm-ui-hover-effect{opacity:.6}.gm-ui-hover-effect:hover{opacity:1}
</style><style type="text/css">.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px;box-sizing:border-box}
</style><style type="text/css">@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><script type="text/javascript" src="./Certy_files/shares.json"></script><script type="text/javascript" src="./Certy_files/count.json"></script><script type="text/javascript" src="./Certy_files/share"></script><script type="text/javascript" src="./Certy_files/saved_resource"></script><script type="text/javascript" src="./Certy_files/shares(1).json"></script><script type="text/javascript" src="./Certy_files/count(1).json"></script><script type="text/javascript" src="./Certy_files/share(1)"></script><script type="text/javascript" src="./Certy_files/saved_resource(1)"></script><style type="text/css">.gm-style-pbc{transition:opacity ease-in-out;background-color:rgba(0,0,0,0.45);text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}
</style><script type="text/javascript" src="./Certy_files/300lo.json"></script><script type="text/javascript" src="./Certy_files/_ate.track.config_resp"></script>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ezequiel Paolillo | Consultor en Tecnología</title>
    <meta name="description" content="">


    <!-- Google Fonts -->
    <link href="./Certy_files/css(1)" rel="stylesheet">
    <link href="./Certy_files/css(2)" rel="stylesheet">

    <!-- Icon Fonts -->
    <link href="./Certy_files/style.css" rel="stylesheet">

    <!-- Styles -->
    <link href="./Certy_files/plugins.min.css" rel="stylesheet">
    <link href="./Certy_files/style.min.css" rel="stylesheet">
    <link href="./Certy_files/custom.css" rel="stylesheet">

    <!-- Modernizer -->
    <script async="" src="./Certy_files/analytics.js"></script><script type="text/javascript" src="./Certy_files/modernizr-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


  <style type="text/css">.at-icon{fill:#fff;border:0}.at-icon-wrapper{display:inline-block;overflow:hidden}a .at-icon-wrapper{cursor:pointer}.at-rounded,.at-rounded-element .at-icon-wrapper{border-radius:12%}.at-circular,.at-circular-element .at-icon-wrapper{border-radius:50%}.addthis_32x32_style .at-icon{width:2pc;height:2pc}.addthis_24x24_style .at-icon{width:24px;height:24px}.addthis_20x20_style .at-icon{width:20px;height:20px}.addthis_16x16_style .at-icon{width:1pc;height:1pc}#at16lb{display:none;position:absolute;top:0;left:0;width:100%;height:100%;z-index:1001;background-color:#000;opacity:.001}#at_complete,#at_error,#at_share,#at_success{position:static!important}.at15dn{display:none}#at15s,#at16p,#at16p form input,#at16p label,#at16p textarea,#at_share .at_item{font-family:arial,helvetica,tahoma,verdana,sans-serif!important;font-size:9pt!important;outline-style:none;outline-width:0;line-height:1em}* html #at15s.mmborder{position:absolute!important}#at15s.mmborder{position:fixed!important;width:250px!important}#at15s{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);float:none;line-height:1em;margin:0;overflow:visible;padding:5px;text-align:left;position:absolute}#at15s a,#at15s span{outline:0;direction:ltr;text-transform:none}#at15s .at-label{margin-left:5px}#at15s .at-icon-wrapper{width:1pc;height:1pc;vertical-align:middle}#at15s .at-icon{width:1pc;height:1pc}.at4-icon{display:inline-block;background-repeat:no-repeat;background-position:top left;margin:0;overflow:hidden;cursor:pointer}.addthis_16x16_style .at4-icon,.addthis_default_style .at4-icon,.at4-icon,.at-16x16{width:1pc;height:1pc;line-height:1pc;background-size:1pc!important}.addthis_32x32_style .at4-icon,.at-32x32{width:2pc;height:2pc;line-height:2pc;background-size:2pc!important}.addthis_24x24_style .at4-icon,.at-24x24{width:24px;height:24px;line-height:24px;background-size:24px!important}.addthis_20x20_style .at4-icon,.at-20x20{width:20px;height:20px;line-height:20px;background-size:20px!important}.at4-icon.circular,.circular .at4-icon,.circular.aticon{border-radius:50%}.at4-icon.rounded,.rounded .at4-icon{border-radius:4px}.at4-icon-left{float:left}#at15s .at4-icon{text-indent:20px;padding:0;overflow:visible;white-space:nowrap;background-size:1pc;width:1pc;height:1pc;background-position:top left;display:inline-block;line-height:1pc}.addthis_vertical_style .at4-icon,.at4-follow-container .at4-icon{margin-right:5px}html>body #at15s{width:250px!important}#at15s.atm{background:none!important;padding:0!important;width:10pc!important}#at15s_inner{background:#fff;border:1px solid #fff;margin:0}#at15s_head{position:relative;background:#f2f2f2;padding:4px;cursor:default;border-bottom:1px solid #e5e5e5}.at15s_head_success{background:#cafd99!important;border-bottom:1px solid #a9d582!important}.at15s_head_success a,.at15s_head_success span{color:#000!important;text-decoration:none}#at15s_brand,#at15sptx,#at16_brand{position:absolute}#at15s_brand{top:4px;right:4px}.at15s_brandx{right:20px!important}a#at15sptx{top:4px;right:4px;text-decoration:none;color:#4c4c4c;font-weight:700}#at15sptx:hover{text-decoration:underline}#at16_brand{top:5px;right:30px;cursor:default}#at_hover{padding:4px}#at_hover .at_item,#at_share .at_item{background:#fff!important;float:left!important;color:#4c4c4c!important}#at_share .at_item .at-icon-wrapper{margin-right:5px}#at_hover .at_bold{font-weight:700;color:#000!important}#at_hover .at_item{width:7pc!important;padding:2px 3px!important;margin:1px;text-decoration:none!important}#at_hover .at_item.athov,#at_hover .at_item:focus,#at_hover .at_item:hover{margin:0!important}#at_hover .at_item.athov,#at_hover .at_item:focus,#at_hover .at_item:hover,#at_share .at_item.athov,#at_share .at_item:hover{background:#f2f2f2!important;border:1px solid #e5e5e5;color:#000!important;text-decoration:none}.ipad #at_hover .at_item:focus{background:#fff!important;border:1px solid #fff}.at15t{display:block!important;height:1pc!important;line-height:1pc!important;padding-left:20px!important;background-position:0 0;text-align:left}.addthis_button,.at15t{cursor:pointer}.addthis_toolbox a.at300b,.addthis_toolbox a.at300m{width:auto}.addthis_toolbox a{margin-bottom:5px;line-height:initial}.addthis_toolbox.addthis_vertical_style{width:200px}.addthis_button_facebook_like .fb_iframe_widget{line-height:100%}.addthis_button_facebook_like iframe.fb_iframe_widget_lift{max-width:none}.addthis_toolbox a.addthis_button_counter,.addthis_toolbox a.addthis_button_facebook_like,.addthis_toolbox a.addthis_button_facebook_send,.addthis_toolbox a.addthis_button_facebook_share,.addthis_toolbox a.addthis_button_foursquare,.addthis_toolbox a.addthis_button_linkedin_counter,.addthis_toolbox a.addthis_button_pinterest_pinit,.addthis_toolbox a.addthis_button_tweet{display:inline-block}.addthis_toolbox span.addthis_follow_label{display:none}.addthis_toolbox.addthis_vertical_style span.addthis_follow_label{display:block;white-space:nowrap}.addthis_toolbox.addthis_vertical_style a{display:block}.addthis_toolbox.addthis_vertical_style.addthis_32x32_style a{line-height:2pc;height:2pc}.addthis_toolbox.addthis_vertical_style .at300bs{margin-right:4px;float:left}.addthis_toolbox.addthis_20x20_style span{line-height:20px}.addthis_toolbox.addthis_32x32_style span{line-height:2pc}.addthis_toolbox.addthis_pill_combo_style .addthis_button_compact .at15t_compact,.addthis_toolbox.addthis_pill_combo_style a{float:left}.addthis_toolbox.addthis_pill_combo_style a.addthis_button_tweet{margin-top:-2px}.addthis_toolbox.addthis_pill_combo_style .addthis_button_compact .at15t_compact{margin-right:4px}.addthis_default_style .addthis_separator{margin:0 5px;display:inline}div.atclear{clear:both}.addthis_default_style .addthis_separator,.addthis_default_style .at4-icon,.addthis_default_style .at300b,.addthis_default_style .at300bo,.addthis_default_style .at300bs,.addthis_default_style .at300m{float:left}.at300b img,.at300bo img{border:0}a.at300b .at4-icon,a.at300m .at4-icon{display:block}.addthis_default_style .at300b,.addthis_default_style .at300bo,.addthis_default_style .at300m{padding:0 2px}.at300b,.at300bo,.at300bs,.at300m{cursor:pointer}.addthis_button_facebook_like.at300b:hover,.addthis_button_facebook_like.at300bs:hover,.addthis_button_facebook_send.at300b:hover,.addthis_button_facebook_send.at300bs:hover{opacity:1}.addthis_20x20_style .at15t,.addthis_20x20_style .at300bs{overflow:hidden;display:block;height:20px!important;width:20px!important;line-height:20px!important}.addthis_32x32_style .at15t,.addthis_32x32_style .at300bs{overflow:hidden;display:block;height:2pc!important;width:2pc!important;line-height:2pc!important}.at300bs{overflow:hidden;display:block;background-position:0 0;height:1pc;width:1pc;line-height:1pc!important}.addthis_default_style .at15t_compact,.addthis_default_style .at15t_expanded{margin-right:4px}#at_share .at_item{width:123px!important;padding:4px;margin-right:2px;border:1px solid #fff}#at16p{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);z-index:10000001;position:absolute;top:50%;left:50%;width:300px;padding:10px;margin:0 auto;margin-top:-185px;margin-left:-155px;font-family:arial,helvetica,tahoma,verdana,sans-serif;font-size:9pt;color:#5e5e5e}#at_share{margin:0;padding:0}#at16pt{position:relative;background:#f2f2f2;height:13px;padding:5px 10px}#at16pt a,#at16pt h4{font-weight:700}#at16pt h4{display:inline;margin:0;padding:0;font-size:9pt;color:#4c4c4c;cursor:default}#at16pt a{position:absolute;top:5px;right:10px;color:#4c4c4c;text-decoration:none;padding:2px}#at15sptx:focus,#at16pt a:focus{outline:thin dotted}#at15s #at16pf a{top:1px}#_atssh{width:1px!important;height:1px!important;border:0!important}.atm{width:10pc!important;padding:0;margin:0;line-height:9pt;letter-spacing:normal;font-family:arial,helvetica,tahoma,verdana,sans-serif;font-size:9pt;color:#444;background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);padding:4px}.atm-f{text-align:right;border-top:1px solid #ddd;padding:5px 8px}.atm-i{background:#fff;border:1px solid #d5d6d6;padding:0;margin:0;box-shadow:1px 1px 5px rgba(0,0,0,.15)}.atm-s{margin:0!important;padding:0!important}.atm-s a:focus{border:transparent;outline:0;transition:none}#at_hover.atm-s a,.atm-s a{display:block;text-decoration:none;padding:4px 10px;color:#235dab!important;font-weight:400;font-style:normal;transition:none}#at_hover.atm-s .at_bold{color:#235dab!important}#at_hover.atm-s a:hover,.atm-s a:hover{background:#2095f0;text-decoration:none;color:#fff!important}#at_hover.atm-s .at_bold{font-weight:700}#at_hover.atm-s a:hover .at_bold{color:#fff!important}.atm-s a .at-label{vertical-align:middle;margin-left:5px;direction:ltr}.at_PinItButton{display:block;width:40px;height:20px;padding:0;margin:0;background-image:url(//s7.addthis.com/static/t00/pinit00.png);background-repeat:no-repeat}.at_PinItButton:hover{background-position:0 -20px}.addthis_toolbox .addthis_button_pinterest_pinit{position:relative}.at-share-tbx-element .fb_iframe_widget span{vertical-align:baseline!important}#at16pf{height:auto;text-align:right;padding:4px 8px}.at-privacy-info{position:absolute;left:7px;bottom:7px;cursor:pointer;text-decoration:none;font-family:helvetica,arial,sans-serif;font-size:10px;line-height:9pt;letter-spacing:.2px;color:#666}.at-privacy-info:hover{color:#000}.body .wsb-social-share .wsb-social-share-button-vert{padding-top:0;padding-bottom:0}.body .wsb-social-share.addthis_counter_style .addthis_button_tweet.wsb-social-share-button{padding-top:40px}.body .wsb-social-share.addthis_counter_style .addthis_button_facebook_like.wsb-social-share-button{padding-top:21px}@media print{#at4-follow,#at4-share,#at4-thankyou,#at4-whatsnext,#at4m-mobile,#at15s,.at4,.at4-recommended{display:none!important}}@media screen and (max-width:400px){.at4win{width:100%}}@media screen and (max-height:700px) and (max-width:400px){.at4-thankyou-inner .at4-recommended-container{height:122px;overflow:hidden}.at4-thankyou-inner .at4-recommended .at4-recommended-item:first-child{border-bottom:1px solid #c5c5c5}}</style><style type="text/css">.at-branding-logo{font-family:helvetica,arial,sans-serif;text-decoration:none;font-size:10px;display:inline-block;margin:2px 0;letter-spacing:.2px}.at-branding-logo .at-branding-icon{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAMAAAC67D+PAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRF////+GlNUkcc1QAAAB1JREFUeNpiYIQDBjQmAwMmkwEM0JnY1WIxFyDAABGeAFEudiZsAAAAAElFTkSuQmCC")}.at-branding-logo .at-branding-icon,.at-branding-logo .at-privacy-icon{display:inline-block;height:10px;width:10px;margin-left:4px;margin-right:3px;margin-bottom:-1px;background-repeat:no-repeat}.at-branding-logo .at-privacy-icon{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAKCAMAAABR24SMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABhQTFRF8fr9ot/xXcfn2/P5AKva////////AKTWodjhjAAAAAd0Uk5T////////ABpLA0YAAAA6SURBVHjaJMzBDQAwCAJAQaj7b9xifV0kUKJ9ciWxlzWEWI5gMF65KUTv0VKkjVeTerqE/x7+9BVgAEXbAWI8QDcfAAAAAElFTkSuQmCC")}.at-branding-logo span{text-decoration:none}.at-branding-logo .at-branding-addthis,.at-branding-logo .at-branding-powered-by{color:#666}.at-branding-logo .at-branding-addthis:hover{color:#333}.at-cv-with-image .at-branding-addthis,.at-cv-with-image .at-branding-addthis:hover{color:#fff}a.at-branding-logo:visited{color:initial}.at-branding-info{display:inline-block;padding:0 5px;color:#666;border:1px solid #666;border-radius:50%;font-size:10px;line-height:9pt;opacity:.7;transition:all .3s ease;text-decoration:none}.at-branding-info span{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.at-branding-info:before{content:'i';font-family:Times New Roman}.at-branding-info:hover{color:#0780df;border-color:#0780df}</style><script src="./Certy_files/jquery.mousewheel.min.js"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/common.js"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/util.js"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/map.js"></script><style type="text/css">.gm-style {
        font: 400 11px Roboto, Arial, sans-serif;
        text-decoration: none;
      }
      .gm-style img { max-width: none; }</style><script type="text/javascript" charset="UTF-8" src="./Certy_files/onion.js"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/stats.js"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/ViewportInfoService.GetViewportInfo"></script><script type="text/javascript" charset="utf-8" async="" src="./Certy_files/layers.7745e3f0a0a2fc3494ea.js"></script><style type="text/css">.at-share-dock.atss{top:auto;left:0;right:0;bottom:0;width:100%;max-width:100%;z-index:1000200;box-shadow:0 0 1px 1px #e2dfe2}.at-share-dock.at-share-dock-zindex-hide{z-index:-1!important}.at-share-dock.atss-top{bottom:auto;top:0}.at-share-dock a{width:auto;transition:none;color:#fff;text-decoration:none;box-sizing:content-box;-webkit-box-sizing:content-box;-moz-box-sizing:content-box}.at-share-dock a:hover{width:auto}.at-share-dock .at4-count{height:43px;padding:5px 0 0;line-height:20px;background:#fff;font-family:Helvetica neue,arial}.at-share-dock .at4-count span{width:100%}.at-share-dock .at4-count .at4-share-label{color:#848484;font-size:10px;letter-spacing:1px}.at-share-dock .at4-count .at4-counter{top:2px;position:relative;display:block;color:#222;font-size:22px}.at-share-dock.at-shfs-medium .at4-count{height:36px;line-height:1pc;padding-top:4px}.at-share-dock.at-shfs-medium .at4-count .at4-counter{font-size:18px}.at-share-dock.at-shfs-medium .at-share-btn .at-icon-wrapper,.at-share-dock.at-shfs-medium a .at-icon-wrapper{padding:6px 0}.at-share-dock.at-shfs-small .at4-count{height:26px;line-height:1;padding-top:3px}.at-share-dock.at-shfs-small .at4-count .at4-share-label{font-size:8px}.at-share-dock.at-shfs-small .at4-count .at4-counter{font-size:14px}.at-share-dock.at-shfs-small .at-share-btn .at-icon-wrapper,.at-share-dock.at-shfs-small a .at-icon-wrapper{padding:4px 0}</style><style type="text/css">div.at-share-close-control.ats-dark,div.at-share-open-control-left.ats-dark,div.at-share-open-control-right.ats-dark{background:#262b30}div.at-share-close-control.ats-light,div.at-share-open-control-left.ats-light,div.at-share-open-control-right.ats-light{background:#fff}div.at-share-close-control.ats-gray,div.at-share-open-control-left.ats-gray,div.at-share-open-control-right.ats-gray{background:#f2f2f2}.atss{position:fixed;top:20%;width:3pc;z-index:100020;background:none}.at-share-close-control{position:relative;width:3pc;overflow:auto}.at-share-open-control-left{position:fixed;top:20%;z-index:100020;left:0;width:22px}.at-share-close-control .at4-arrow.at-left{float:right}.atss-left{left:0;float:left;right:auto}.atss-right{left:auto;float:right;right:0}.atss-right.at-share-close-control .at4-arrow.at-right{position:relative;right:0;overflow:auto}.atss-right.at-share-close-control .at4-arrow{float:left}.at-share-open-control-right{position:fixed;top:20%;z-index:100020;right:0;width:22px;float:right}.atss-right .at-share-close-control .at4-arrow{float:left}.atss.atss-right a{float:right}.atss.atss-right .at4-share-title{float:right;overflow:hidden}.atss .at-share-btn,.atss a{position:relative;display:block;width:3pc;margin:0;outline-offset:-1px;text-align:center;float:left;transition:width .15s ease-in-out;overflow:hidden;background:#e8e8e8;z-index:100030;cursor:pointer}.at-share-btn::-moz-focus-inner{border:0;padding:0}.atss-right .at-share-btn{float:right}.atss .at-share-btn{border:0;padding:0}.atss .at-share-btn:focus,.atss .at-share-btn:hover,.atss a:focus,.atss a:hover{width:4pc}.atss .at-share-btn .at-icon-wrapper,.atss a .at-icon-wrapper{display:block;padding:8px 0}.atss .at-share-btn:last-child,.atss a:last-child{border:none}.atss .at-share-btn span .at-icon,.atss a span .at-icon{position:relative;top:0;left:0;display:block;background-repeat:no-repeat;background-position:50% 50%;width:2pc;height:2pc;line-height:2pc;border:none;padding:0;margin:0 auto;overflow:hidden;cursor:pointer;cursor:hand}.at4-share .at-custom-sidebar-counter{font-family:Helvetica neue,arial;vertical-align:top;margin-right:4px;display:inline-block;text-align:center}.at4-share .at-custom-sidebar-count{font-size:17px;line-height:1.25em;color:#222}.at4-share .at-custom-sidebar-text{font-size:9px;line-height:1.25em;color:#888;letter-spacing:1px}.at4-share .at4-share-count-container{position:absolute;left:0;right:auto;top:auto;bottom:0;width:100%;color:#fff;background:inherit}.at4-share .at4-share-count,.at4-share .at4-share-count-container{line-height:1pc;font-size:10px}.at4-share .at4-share-count{text-indent:0;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-weight:200;width:100%;height:1pc}.at4-share .at4-share-count-anchor{padding-bottom:8px;text-decoration:none;transition:padding .15s ease-in-out .15s,width .15s ease-in-out}</style><style type="text/css">#at4-drawer-outer-container{top:0;width:20pc;position:fixed}#at4-drawer-outer-container.at4-drawer-inline{position:relative}#at4-drawer-outer-container.at4-drawer-inline.at4-drawer-right{float:right;right:0;left:auto}#at4-drawer-outer-container.at4-drawer-inline.at4-drawer-left{float:left;left:0;right:auto}#at4-drawer-outer-container.at4-drawer-shown,#at4-drawer-outer-container.at4-drawer-shown *{z-index:999999}#at4-drawer-outer-container,#at4-drawer-outer-container .at4-drawer-outer,#at-drawer{height:100%;overflow-y:auto;overflow-x:hidden}.at4-drawer-push-content-right-back{position:relative;right:0}.at4-drawer-push-content-right{position:relative;left:20pc!important}.at4-drawer-push-content-left-back{position:relative;left:0}.at4-drawer-push-content-left{position:relative;right:20pc!important}#at4-drawer-outer-container.at4-drawer-right{left:auto;right:-20pc}#at4-drawer-outer-container.at4-drawer-left{right:auto;left:-20pc}#at4-drawer-outer-container.at4-drawer-shown.at4-drawer-right{left:auto;right:0}#at4-drawer-outer-container.at4-drawer-shown.at4-drawer-left{right:auto;left:0}#at-drawer{top:0;z-index:9999999;height:100%;animation-duration:.4s}#at-drawer.drawer-push.at-right{right:-20pc}#at-drawer.drawer-push.at-left{left:-20pc}#at-drawer .at-recommended-label{padding:0 0 0 20px;color:#999;line-height:3pc;font-size:18px;font-weight:300;cursor:default}#at-drawer-arrow{width:30px;height:5pc}#at-drawer-arrow.ats-dark{background:#262b30}#at-drawer-arrow.ats-gray{background:#f2f2f2}#at-drawer-open-arrow{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAABcCAYAAAC1OT8uAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjk3ODNCQjdERUQ3QjExRTM5NjFGRUZBODc3MTIwMTNCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjk3ODNCQjdFRUQ3QjExRTM5NjFGRUZBODc3MTIwMTNCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTc4M0JCN0JFRDdCMTFFMzk2MUZFRkE4NzcxMjAxM0IiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OTc4M0JCN0NFRDdCMTFFMzk2MUZFRkE4NzcxMjAxM0IiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7kstzCAAAB4ElEQVR42uyWv0oDQRDGb9dYimgVjliID2Ca9AGfwtZob2Grja1PIFj7EhGCYK99VPBPOkVMp8X5rc6FeN7dfjOksMjAxwXZ3667OzvfLKRr682l5ZV9aDh+fxsnRHhoDzqGLjFBi4XOoFtoAxowoB893o/w7WpAl/+QgQMBwwRdTPhUC2lAV/wDA7qy5WOgq9psHejqTqkKdLE7KYCv0JZjMgBgB58raBG6mP1K6j2pT099T+qMUOeeOss1wDcEIA1PnQXy576rAUI0oFMoC7VCnn40Gs8Pd4lAiXNUKmJ0lh1mPzGEWiyUCqAGW3Pwv4IvUJsFO9CHgP3Zr6Te0xwgAf3LxaAjS241pbikCRkOg+nSJdV4p8HOPl3vvRYI5dtrgVDvvcWovcWovcWovcWovcWovcWovQChWNywNpqvdAKtQp/QNmPUIQ6kwwqt2Xmsxf6GMPM1Pptsbz45CPmXqKb+15Gz4J/LZcDSNIqBlQlbB0afe1mmUDWiCNKFZRq0VKMeXY1CTDq2sJLWsCmoaBBRqNRR6qBKC6qCaj2rDIqaXBGiXHEaom00h1S+K3fVlr6HNuqgvgCh0+owt21bybQn8+mZ78mcEebcM2e5+T2ZX24ZqCph0qn1vgQYAJ/KDpLQr2tPAAAAAElFTkSuQmCC);background-repeat:no-repeat;width:13px;height:23px;margin:28px 0 0 8px}.at-left #at-drawer-open-arrow{background-position:0 -46px}.ats-dark #at-drawer-open-arrow{background-position:0 -23px}.ats-dark.at-left #at-drawer-open-arrow{background-position:0 -69px}#at-drawer-arrow.at4-drawer-modern-browsers{position:fixed;top:40%;background-repeat:no-repeat;background-position:0 0!important;z-index:9999999}.at4-drawer-inline #at-drawer-arrow{position:absolute}#at-drawer-arrow.at4-drawer-modern-browsers.at-right{right:0}#at-drawer-arrow.at4-drawer-modern-browsers.at-left{left:0}.at4-drawer-push-animation-left{transition:left .4s ease-in-out .15s}.at4-drawer-push-animation-right{transition:right .4s ease-in-out .15s}#at-drawer.drawer-push.at4-drawer-push-animation-right{right:0}#at-drawer.drawer-push.at4-drawer-push-animation-right-back{right:-20pc!important}#at-drawer.drawer-push.at4-drawer-push-animation-left{left:0}#at-drawer.drawer-push.at4-drawer-push-animation-left-back{left:-20pc!important}#at-drawer .at4-closebutton.drawer-close{content:'X';color:#999;display:block;position:absolute;margin:0;top:0;right:0;width:3pc;height:45px;line-height:45px;overflow:hidden;opacity:.5}#at-drawer.ats-dark .at4-closebutton.drawer-close{color:#fff}#at-drawer .at4-closebutton.drawer-close:hover{opacity:1}#at-drawer.ats-dark.at4-recommended .at4-logo-container a{color:#666}#at-drawer.at4-recommended .at4-recommended-vertical{padding:0}#at-drawer.at4-recommended .at4-recommended-item .sponsored-label{margin:2px 0 0 21px;color:#ddd}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item{position:relative;padding:0;width:20pc;height:180px;margin:0}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img a:after{content:'';position:absolute;top:0;left:0;right:0;bottom:0;background:rgba(0,0,0,.65);z-index:1000000;transition:all .2s ease-in-out}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item.at-hover .at4-recommended-item-img a:after{background:rgba(0,0,0,.8)}#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img,#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img a,#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img img{width:20pc;height:180px;float:none}#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption{width:100%;position:absolute;bottom:0;left:0;height:70px}#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption .at-h4{color:#fff;position:absolute;height:52px;top:0;left:20px;right:20px;margin:0;padding:0;line-height:25px;font-size:20px;font-weight:600;z-index:1000001;text-decoration:none;text-transform:none}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption .at-h4 a:hover{text-decoration:none}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption .at-h4 a:link{color:#fff}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption small{position:absolute;top:auto;bottom:10px;left:20px;width:auto;color:#ccc}#at-drawer.at4-recommended .at4-logo-container{margin-left:20px}#at-drawer.ats-dark.at4-recommended .at4-logo-container a:hover{color:#fff}#at-drawer.at4-recommended .at-logo{margin:0}</style><style type="text/css">.at4-follow.at-mobile{display:none!important}.at4-follow{position:fixed;top:0;right:0;font-weight:400;color:#666;cursor:default;z-index:10001}.at4-follow .at4-follow-inner{position:relative;padding:10px 24px 10px 15px}.at4-follow-inner,.at-follow-open-control{border:0 solid #c5c5c5;border-width:1px 0 1px 1px;margin-top:-1px}.at4-follow .at4-follow-container{margin-left:9pt}.at4-follow.at4-follow-24 .at4-follow-container{height:24px;line-height:23px;font-size:13px}.at4-follow.at4-follow-32 .at4-follow-container{width:15pc;height:2pc;line-height:2pc;font-size:14px}.at4-follow .at4-follow-container .at-follow-label{display:inline-block;height:24px;line-height:24px;margin-right:10px;padding:0;cursor:default;float:left}.at4-follow .at4-follow-container .at-icon-wrapper{height:24px;width:24px}.at4-follow.ats-transparent .at4-follow-inner,.at-follow-open-control.ats-transparent{border-color:transparent}.at4-follow.ats-dark .at4-follow-inner,.at-follow-open-control.ats-dark{background:#262b30;border-color:#000;color:#fff}.at4-follow.ats-dark .at-follow-close-control{background-color:#262b30}.at4-follow.ats-light .at4-follow-inner{background:#fff;border-color:#c5c5c5}.at4-follow.ats-gray .at4-follow-inner,.at-follow-open-control.ats-gray{background:#f2f2f2;border-color:#c5c5c5}.at4-follow.ats-light .at4-follow-close-control,.at-follow-open-control.ats-light{background:#e5e5e5}.at4-follow .at4-follow-inner .at4-follow-close-control{position:absolute;top:0;bottom:0;left:0;width:20px;cursor:pointer;display:none}.at4-follow .at4-follow-inner .at4-follow-close-control div{display:block;line-height:20px;text-indent:-9999em;margin-top:calc(50% + 1px);overflow:hidden}.at-follow-open-control div.at4-arrow.at-left{background-position:0 -2px}.at-follow-open-control{position:fixed;height:35px;top:0;right:0;padding-top:10px;z-index:10002}.at-follow-btn{margin:0 5px 5px 0;padding:0;outline-offset:-1px;display:inline-block;box-sizing:content-box;transition:all .2s ease-in-out}.at-follow-btn:focus,.at-follow-btn:hover{transform:translateY(-4px)}.at4-follow-24 .at-follow-btn{height:25px;line-height:0;width:25px}</style><style type="text/css">.at-follow-tbx-element .at300b,.at-follow-tbx-element .at300m{display:inline-block;width:auto;padding:0;margin:0 2px 5px;outline-offset:-1px;transition:all .2s ease-in-out}.at-follow-tbx-element .at300b:focus,.at-follow-tbx-element .at300b:hover,.at-follow-tbx-element .at300m:focus,.at-follow-tbx-element .at300m:hover{transform:translateY(-4px)}.at-follow-tbx-element .addthis_vertical_style .at300b,.at-follow-tbx-element .addthis_vertical_style .at300m{display:block}.at-follow-tbx-element .addthis_vertical_style .at300b .addthis_follow_label,.at-follow-tbx-element .addthis_vertical_style .at300b .at-icon-wrapper,.at-follow-tbx-element .addthis_vertical_style .at300m .addthis_follow_label,.at-follow-tbx-element .addthis_vertical_style .at300m .at-icon-wrapper{display:inline-block;vertical-align:middle;margin-right:5px}.at-follow-tbx-element .addthis_vertical_style .at300b:focus,.at-follow-tbx-element .addthis_vertical_style .at300b:hover,.at-follow-tbx-element .addthis_vertical_style .at300m:focus,.at-follow-tbx-element .addthis_vertical_style .at300m:hover{transform:none}</style><style type="text/css">.at4-jumboshare .at-share-btn{display:inline-block;margin-right:13px;margin-top:13px}.at4-jumboshare .at-share-btn .at-icon{float:left}.at4-jumboshare .at-share-btn .at300bs{display:inline-block;float:left;cursor:pointer}.at4-jumboshare .at4-mobile .at-share-btn .at-icon,.at4-jumboshare .at4-mobile .at-share-btn .at-icon-wrapper{margin:0;padding:0}.at4-jumboshare .at4-mobile .at-share-btn{padding:0}.at4-jumboshare .at4-mobile .at-share-btn .at-label{display:none}.at4-jumboshare .at4-count{font-size:60px;line-height:60px;font-family:Helvetica neue,arial;font-weight:700}.at4-jumboshare .at4-count-container{display:table-cell;text-align:center;min-width:200px;vertical-align:middle;border-right:1px solid #ccc;padding-right:20px}.at4-jumboshare .at4-share-container{display:table-cell;vertical-align:middle;padding-left:20px}.at4-jumboshare .at4-share-container.at-share-tbx-element{padding-top:0}.at4-jumboshare .at4-title{position:relative;font-size:18px;line-height:18px;bottom:2px}.at4-jumboshare .at4-spacer{height:1px;display:block;visibility:hidden;opacity:0}.at4-jumboshare .at-share-btn{display:inline-block;margin:0 2px;line-height:0;padding:0;overflow:hidden;text-decoration:none;text-transform:none;color:#fff;cursor:pointer;transition:all .2s ease-in-out;border:0;background-color:transparent}.at4-jumboshare .at-share-btn:focus,.at4-jumboshare .at-share-btn:hover{transform:translateY(-4px);color:#fff;text-decoration:none}.at4-jumboshare .at-label{font-family:helvetica neue,helvetica,arial,sans-serif;font-size:9pt;padding:0 15px 0 0;margin:0;height:2pc;line-height:2pc;background:none}.at4-jumboshare .at-share-btn:hover,.at4-jumboshare .at-share-btn:link{text-decoration:none}.at4-jumboshare .at-share-btn::-moz-focus-inner{border:0;padding:0}.at4-jumboshare.at-mobile .at-label{display:none}</style><style type="text/css">.at4-recommendedbox-outer-container{display:inline}.at4-recommended-outer{position:static}.at4-recommended{top:20%;margin:0;text-align:center;font-weight:400;font-size:13px;line-height:17px;color:#666}.at4-recommended.at-inline .at4-recommended-horizontal{text-align:left}.at4-recommended-recommendedbox{padding:0;z-index:inherit}.at4-recommended-recommended{padding:40px 0}.at4-recommended-horizontal{max-height:340px}.at4-recommended.at-medium .at4-recommended-horizontal{max-height:15pc}.at4-recommended.at4-minimal.at-medium .at4-recommended-horizontal{padding-top:10px;max-height:230px}.at4-recommended-text-only .at4-recommended-horizontal{max-height:130px}.at4-recommended-horizontal{padding-top:5px;overflow-y:hidden}.at4-minimal{background:none;color:#000;border:none!important;box-shadow:none!important}@media screen and (max-width:900px){.at4-recommended-horizontal .at4-recommended-item,.at4-recommended-horizontal .at4-recommended-item .at4-recommended-item-img{width:15pc}}.at4-recommended.at4-minimal .at4-recommended-horizontal .at4-recommended-item .at4-recommended-item-caption{padding:0 0 10px}.at4-recommended.at4-minimal .at4-recommended-horizontal .at4-recommended-item-caption{padding:20px 0 0!important}.addthis-smartlayers .at4-recommended .at-h3.at-recommended-label{margin:0;padding:0;font-weight:300;font-size:18px;line-height:24px;color:#464646;width:100%;display:inline-block;zoom:1}.addthis-smartlayers .at4-recommended.at-inline .at-h3.at-recommended-label{text-align:left}#at4-thankyou .addthis-smartlayers .at4-recommended.at-inline .at-h3.at-recommended-label{text-align:center}.at4-recommended .at4-recommended-item{display:inline-block;zoom:1;position:relative;background:#fff;border:1px solid #c5c5c5;width:200px;margin:10px}.addthis_recommended_horizontal .at4-recommended-item{border:none}.at4-recommended .at4-recommended-item .sponsored-label{color:#666;font-size:9px;position:absolute;top:-20px}.at4-recommended .at4-recommended-item-img .at-tli,.at4-recommended .at4-recommended-item-img a{position:absolute;left:0}.at4-recommended.at-inline .at4-recommended-horizontal .at4-recommended-item{margin:10px 20px 0 0}.at4-recommended.at-medium .at4-recommended-horizontal .at4-recommended-item{margin:10px 10px 0 0}.at4-recommended.at-medium .at4-recommended-item{width:140px;overflow:hidden}.at4-recommended .at4-recommended-item .at4-recommended-item-img{position:relative;text-align:center;width:100%;height:200px;line-height:0;overflow:hidden}.at4-recommended .at4-recommended-item .at4-recommended-item-img a{display:block;width:100%;height:200px}.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-img,.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-img a{height:140px}.at4-recommended .at4-recommended-item .at4-recommended-item-img img{position:absolute;top:0;left:0;min-height:0;min-width:0;max-height:none;max-width:none;margin:0;padding:0}.at4-recommended .at4-recommended-item .at4-recommended-item-caption{height:74px;overflow:hidden;padding:20px;text-align:left;-ms-box-sizing:content-box;-o-box-sizing:content-box;box-sizing:content-box}.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-caption{height:50px;padding:15px}.at4-recommended .at4-recommended-item .at4-recommended-item-caption .at-h4{height:54px;margin:0 0 5px;padding:0;overflow:hidden;word-wrap:break-word;font-size:14px;font-weight:400;line-height:18px;text-align:left}.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-caption .at-h4{font-size:9pt;line-height:1pc;height:33px}.at4-recommended .at4-recommended-item:hover .at4-recommended-item-caption .at-h4{text-decoration:underline}.at4-recommended a:link,.at4-recommended a:visited{text-decoration:none;color:#464646}.at4-recommended .at4-recommended-item .at4-recommended-item-caption .at-h4 a:hover{text-decoration:underline;color:#000}.at4-recommended .at4-recommended-item .at4-recommended-item-caption small{display:block;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;font-size:11px;color:#666}.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-caption small{font-size:9px}.at4-recommended .at4-recommended-vertical{padding:15px 0 0}.at4-recommended .at4-recommended-vertical .at4-recommended-item{display:block;width:auto;max-width:100%;height:60px;border:none;margin:0 0 15px;box-shadow:none;background:none}.at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img,.at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img img{width:60px;height:60px;float:left}.at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption{border-top:none;margin:0;height:60px;padding:3px 5px}.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption .at-h4{height:38px;margin:0}.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption small{position:absolute;bottom:0}.at4-recommended .at-recommended-label.at-vertical{text-align:left}.at4-no-image-light-recommended,.at4-no-image-minimal-recommended{background-color:#f2f2f2!important}.at4-no-image-gray-recommended{background-color:#e6e6e5!important}.at4-no-image-dark-recommended{background-color:#4e555e!important}.at4-recommended .at4-recommended-item-placeholder-img{background-repeat:no-repeat!important;background-position:center!important;width:100%!important;height:100%!important}.at4-recommended-horizontal .at4-no-image-dark-recommended .at4-recommended-item-placeholder-img{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAfCAYAAACCox+xAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjlFNUUyQTg3MTI0RDExRTM4NzAwREJDRjlCQzAyMUVFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjlFNUUyQTg4MTI0RDExRTM4NzAwREJDRjlCQzAyMUVFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OUU1RTJBODUxMjREMTFFMzg3MDBEQkNGOUJDMDIxRUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OUU1RTJBODYxMjREMTFFMzg3MDBEQkNGOUJDMDIxRUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6oCfPiAAABfUlEQVR42uyWTU/DMAyGm3bdBxp062hHe+PC//9HCIkDYpNAO7CPAuWN5Eohyhpno2GHWqq8pO78xHHsiLquH4L/l6cwuBAZaOPKs//YBFIJIR59UiAt7huYi90aE/UQakTDLaL26RUEAAJqiefm93T9Bpj1X4O0bY0OIUXCpYBJvYDAUWyAUCWliHGTcnpqRMaM72ImRAJVknYG+eb4YEDIBeU0zGnsBLK1ODogYSsLhDwIJeVVk18lzfNA4ERGZNXi59UCIQhiYDilpSm/jp4awLxDvWhlf4/nGe8+LLuSt+SZul28ggaHG6gNVhDR+IuRFzOoxGKWwG7vVFm5AAQxgcqYpzrjFjR9zwPH5LSuT7XlNr2MQm5LzqjLpncNNaM+s8M27Y60g3FwhoSMzrtUQllgLtRs5pZ2cB4IhbvQbGRZv1NsrhyS8+SI5Mo9RJWpjAI1xqKL+0iEP180vy214JbeR12AyOgsHI7e0NfFyKv0ID1ID+IqPwIMAOeljGQOryBmAAAAAElFTkSuQmCC)!important}.at4-recommended-vertical .at4-no-image-dark-recommended .at4-recommended-item-placeholder-img{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAOCAYAAADwikbvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjAzREMyNTM2MTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjAzREMyNTM3MTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDNEQzI1MzQxMjRGMTFFMzg3MDBEQkNGOUJDMDIxRUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDNEQzI1MzUxMjRGMTFFMzg3MDBEQkNGOUJDMDIxRUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5GfbtkAAAAxklEQVR42qRSTQvCMAxduk53mEOHKFPP/v8/5cGTiIibivVFUomlG7gFHvloXpKmJefcPhkmNyvGEWj+IOZA6ckPImoxxVwOLvCvXUzkpayNCpRQK64IbOBnAYGAXMeMslNlU+CzrIEdCkxi5DPAoz6BE8ZuVNdKJuL8rS9sv62IXlCHyP0KqKUKZXK9uwkSLVArfwpVR3b225kXwovibcP+jC4jUtfWPZmfqJJnYlkAM128j1z0nHWKSUbIKDL/msHktwADAPptQo+vkZNLAAAAAElFTkSuQmCC)!important}.at4-recommended-horizontal .at4-no-image-gray-recommended .at4-recommended-item-placeholder-img,.at4-recommended-horizontal .at4-no-image-light-recommended .at4-recommended-item-placeholder-img,.at4-recommended-horizontal .at4-no-image-minimal-recommended .at4-recommended-item-placeholder-img{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAfCAYAAACCox+xAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjAzREMyNTMyMTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjAzREMyNTMzMTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OUU1RTJBODkxMjREMTFFMzg3MDBEQkNGOUJDMDIxRUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OUU1RTJBOEExMjREMTFFMzg3MDBEQkNGOUJDMDIxRUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6dfDQvAAABg0lEQVR42uyWS0vDQBDH82jaKNW0qUltbl68e/Di98eLBz+CCB5EBaWIpUat/4UJLMuame1j7SEDYbqbKfPLvHbDi8ur8+D/5T4K9kR6xrr27D+xgdS3N9d3PilQFmcNzN6mxkbdhxrQcoGofXkFAUAINcVzrG2vsP8KmJdtg7SlxoRQouBywOReQOAosUDoklPEpEU5XDciqeB/iRAig6pIO4P8CHysBBDqg0palrR2Alkwjj5RsDUDoRqhorpq6quifRkInKiIPLf4eWIgQoLoWbq0stXXn10DmDeoR2PsL/E84N0Hk5Wypc70dMkGGhzOoeb4gpjW34K6GEFljFkGu6XTZJUCEMQBVCHs6kI60MycB47FyUmo20oPvYJCzhVnvIsR3zg5ghoRTNpyHKTBBhIJTt6pFsoZ9iLDZswcB5uBULhnho0a66eazaFDca59Hym1e4guQ4rCO4Fu/T4Sw8Gk+c3MghN6H+8CRKVg4tB6fV8XI6/SgXQgHYir/AowAMU5TskhKVUNAAAAAElFTkSuQmCC)!important}.at4-recommended-vertical .at4-no-image-gray-recommended .at4-recommended-item-placeholder-img,.at4-recommended-vertical .at4-no-image-light-recommended .at4-recommended-item-placeholder-img,.at4-recommended-vertical .at4-no-image-minimal-recommended .at4-recommended-item-placeholder-img{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAOCAYAAADwikbvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjAzREMyNTNBMTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjAzREMyNTNCMTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDNEQzI1MzgxMjRGMTFFMzg3MDBEQkNGOUJDMDIxRUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDNEQzI1MzkxMjRGMTFFMzg3MDBEQkNGOUJDMDIxRUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz65Fr9cAAAA0ElEQVR42qRRuQrCQBDd3SSaIgYNosSrtLew8f+xsfAnYmEVRMR4YHwjExjCbsBk4DHHzptjR2+2u7VqJ3efjTNQ/EEMgbgiv46H/QNTDPnhCv/mYiLPI21EIIaaUEVgBj+oETQQypgRtidsXfNJpsACBXo28gWgUd9AjrEL0TXhiSh/XhWudlZI/kCdLPtFUGMRCni9p6kl+kAq/D5UavmzX2fNd87obsCSfztnrOR0rjvTiRImkoyAQQNRyZ2jhjenGNVBOpF1WZatyV8BBgBJ+irgS/KHdAAAAABJRU5ErkJggg==)!important}#at-drawer.ats-dark,.at4-recommended.ats-dark .at4-recommended-horizontal .at4-recommended-item-caption,.at4-recommended.ats-dark .at4-recommended-vertical .at4-recommended-item-caption{background:#262b30}#at-drawer.ats-gray,.at4-recommended.ats-gray .at4-recommended-horizontal .at4-recommended-item-caption{background:#f2f2f2}#at-drawer.ats-light,.at4-recommended.ats-light .at4-recommended-horizontal .at4-recommended-item-caption{background:#fff}.at4-recommended.ats-dark .at4-recommended-vertical .at4-recommended-item{background:none}.at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption a:hover,.at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption a:link,.at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption a:visited,.at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption small,.at4-recommended.ats-dark .at4-recommended-item-caption,.at4-recommended.ats-dark .at-logo a:hover,.at4-recommended.ats-dark .at-recommended-label.at-vertical{color:#fff}.at4-recommended-vertical-logo{padding-top:0;text-align:left}.at4-recommended-vertical-logo .at4-logo-container{line-height:10px}.at4-recommended-horizontal-logo{text-align:center}.at4-recommended.at-inline .at4-recommended-horizontal-logo{text-align:left}#at4-thankyou .at4-recommended.at-inline .at4-recommended-horizontal{text-align:center}.at4-recommended .at-logo{margin:10px 0 0;padding:0;height:25px;overflow:auto;-ms-box-sizing:content-box;-o-box-sizing:content-box;box-sizing:content-box}.at4-recommended.at-inline .at4-recommended-horizontal .at-logo{text-align:left}.at4-recommended .at4-logo-container a.at-sponsored-link{color:#666}.at4-recommended-class .at4-logo-container a:hover,.at4-recommendedbox-outer-container .at4-recommended-recommendedbox .at4-logo-container a:hover{color:#000}</style><style type="text/css">.at-recommendedjumbo-outer-container{margin:0;padding:0;border:0;background:none;color:#000}.at-recommendedjumbo-footer{position:relative;width:100%;height:510px;overflow:hidden;transition:all .3s ease-in-out}.at-mobile .at-recommendedjumbo-footer{height:250px}.at-recommendedjumbo-footer #bg-link:after{content:'';position:absolute;top:0;left:0;right:0;bottom:0;background:rgba(0,0,0,.75)}.at-recommendedjumbo-footer:hover #bg-link:after{background:rgba(0,0,0,.85)}.at-recommendedjumbo-footer *,.at-recommendedjumbo-footer :after,.at-recommendedjumbo-footer :before{box-sizing:border-box}.at-recommendedjumbo-footer:hover #at-recommendedjumbo-footer-bg{animation:atRecommendedJumboAnimatedBackground 1s ease-in-out 1;animation-fill-mode:forwards}.at-recommendedjumbo-footer #at-recommendedjumbo-top-holder{position:absolute;top:0;padding:0 40px;width:100%}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-top-holder{padding:0 20px}.at-recommendedjumbo-footer .at-recommendedjumbo-footer-inner{position:relative;text-align:center;font-family:helvetica,arial,sans-serif;z-index:2;width:100%}.at-recommendedjumbo-footer #at-recommendedjumbo-label-holder{margin:40px 0 0;max-height:30px}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-label-holder{margin:20px 0 0;max-height:20px}.at-recommendedjumbo-footer #at-recommendedjumbo-label{font-weight:300;font-size:24px;line-height:24px;color:#fff;margin:0}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-label{font-weight:150;font-size:14px;line-height:14px}.at-recommendedjumbo-footer #at-recommendedjumbo-title-holder{margin:20px 0 0;min-height:3pc;max-height:78pt}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-title-holder{margin:10px 0 0;min-height:24px;max-height:54px}.at-recommendedjumbo-footer #at-recommendedjumbo-content-title{font-size:3pc;line-height:52px;font-weight:700;margin:0}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-content-title{font-size:24px;line-height:27px}.at-recommendedjumbo-footer a{text-decoration:none;color:#fff}.at-recommendedjumbo-footer a:visited{color:#fff}.at-recommendedjumbo-footer small{margin:20px 0 0;display:inline-block;height:2pc;line-height:2pc;font-size:14px;color:#ccc;cursor:default}.at-mobile .at-recommendedjumbo-footer small{margin:10px 0 0;height:14px;line-height:14px;font-size:9pt}.at-recommendedjumbo-footer .at-logo-container{position:absolute;bottom:20px;margin:auto;left:0;right:0}.at-mobile .at-recommendedjumbo-footer .at-logo-container{bottom:10px}.at-recommendedjumbo-footer a.at-sponsored-link{color:#ccc}.at-recommendedjumbo-footer div #at-recommendedjumbo-logo-link{padding:2px 0 0 11px;text-decoration:none;line-height:20px;font-family:helvetica,arial,sans-serif;font-size:9px;color:#ccc}@keyframes atRecommendedJumboAnimatedBackground{0%{transform:scale(1,1)}to{transform:scale(1.1,1.1)}}</style><style type="text/css">.at-resp-share-element{position:relative;padding:0;margin:0;font-size:0;line-height:0}.at-resp-share-element:after,.at-resp-share-element:before{content:" ";display:table}.at-resp-share-element.at-mobile .at4-share-count-container,.at-resp-share-element.at-mobile .at-label{display:none}.at-resp-share-element .at-share-btn{display:inline-block;*display:inline;*zoom:1;margin:0 2px 5px;padding:0;overflow:hidden;line-height:0;text-decoration:none;text-transform:none;color:#fff;cursor:pointer;transition:all .2s ease-in-out;border:0;font-family:helvetica neue,helvetica,arial,sans-serif;background-color:transparent}.at-resp-share-element .at-share-btn::-moz-focus-inner{border:0;padding:0}.at-resp-share-element .at-share-btn:focus,.at-resp-share-element .at-share-btn:hover{transform:translateY(-4px);color:#fff;text-decoration:none}.at-resp-share-element .at-share-btn .at-icon-wrapper{float:left}.at-resp-share-element .at-share-btn.at-share-btn.at-svc-compact:hover{transform:none}.at-resp-share-element .at-share-btn .at-label{font-family:helvetica neue,helvetica,arial,sans-serif;font-size:9pt;padding:0 15px 0 0;margin:0 0 0 5px;height:2pc;line-height:2pc;background:none}.at-resp-share-element .at-icon,.at-resp-share-element .at-label{cursor:pointer}.at-resp-share-element .at4-share-count-container{text-decoration:none;float:right;padding-right:15px;font-size:9pt}.at-mobile .at-resp-share-element .at-label{display:none}.at-resp-share-element.at-mobile .at-share-btn{margin-right:5px}.at-mobile .at-resp-share-element .at-share-btn{padding:5px;margin-right:5px}</style><style type="text/css">.at-share-tbx-element{position:relative;margin:0;color:#fff;font-size:0}.at-share-tbx-element,.at-share-tbx-element .at-share-btn{font-family:helvetica neue,helvetica,arial,sans-serif;padding:0;line-height:0}.at-share-tbx-element .at-share-btn{cursor:pointer;margin:0 5px 5px 0;display:inline-block;overflow:hidden;border:0;text-decoration:none;text-transform:none;background-color:transparent;color:inherit;transition:all .2s ease-in-out}.at-share-tbx-element .at-share-btn:focus,.at-share-tbx-element .at-share-btn:hover{transform:translateY(-4px);outline-offset:-1px;color:inherit}.at-share-tbx-element .at-share-btn::-moz-focus-inner{border:0;padding:0}.at-share-tbx-element .at-share-btn.at-share-btn.at-svc-compact:hover{transform:none}.at-share-tbx-element .at-icon-wrapper{vertical-align:middle}.at-share-tbx-element .at4-share-count,.at-share-tbx-element .at-label{margin:0 7.5px 0 2.5px;text-decoration:none;vertical-align:middle;display:inline-block;background:none;height:0;font-size:inherit;line-height:inherit;color:inherit}.at-share-tbx-element.at-mobile .at4-share-count,.at-share-tbx-element.at-mobile .at-label{display:none}.at-share-tbx-element .at_native_button{vertical-align:middle}.at-share-tbx-element .addthis_counter.addthis_bubble_style{margin:0 2px;vertical-align:middle;display:inline-block}.at-share-tbx-element .fb_iframe_widget{display:block}.at-share-tbx-element.at-share-tbx-native .at300b{vertical-align:middle}.at-style-responsive .at-share-btn{padding:5px}.at-style-jumbo{display:table}.at-style-jumbo .at4-spacer{height:1px;display:block;visibility:hidden;opacity:0}.at-style-jumbo .at4-count-container{display:table-cell;text-align:center;min-width:200px;vertical-align:middle;border-right:1px solid #ccc;padding-right:20px}.at-style-jumbo .at4-count{font-size:60px;line-height:60px;font-weight:700}.at-style-jumbo .at4-count-title{position:relative;font-size:18px;line-height:18px;bottom:2px}.at-style-jumbo .at-share-btn-elements{display:table-cell;vertical-align:middle;padding-left:20px}.at_flat_counter{cursor:pointer;font-family:helvetica,arial,sans-serif;font-weight:700;text-transform:uppercase;display:inline-block;position:relative;vertical-align:top;height:auto;margin:0 5px;padding:0 6px;left:-1px;background:#ebebeb;color:#32363b;transition:all .2s ease}.at_flat_counter:after{top:30%;left:-4px;content:"";position:absolute;border-width:5px 8px 5px 0;border-style:solid;border-color:transparent #ebebeb transparent transparent;display:block;width:0;height:0;transform:translateY(360deg)}.at_flat_counter:hover{background:#e1e2e2}</style><style type="text/css">.at4-thankyou-background{top:0;right:0;left:0;bottom:0;-webkit-overflow-scrolling:touch;z-index:9999999;background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpizCuu/sRABGBiIBKMKqSOQoAAAwC8KgJipENhxwAAAABJRU5ErkJggg==);background:hsla(217,6%,46%,.95)}.at4-thankyou-background.at-thankyou-shown{position:fixed}.at4-thankyou-inner{position:absolute;width:100%;top:10%;left:50%;margin-left:-50%;text-align:center}.at4-thankyou-mobile .at4-thankyou-inner{top:5%}.thankyou-description{font-weight:400}.at4-thankyou-background .at4lb-inner{position:relative;width:100%;height:100%}.at4-thankyou-background .at4lb-inner .at4x{position:absolute;top:15px;right:15px;display:block;width:20px;height:20px;padding:20px;margin:0;cursor:pointer;transition:opacity .25s ease-in;opacity:.4;background:url("data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAAAAWdEVYdENyZWF0aW9uIFRpbWUAMTEvMTMvMTKswDp5AAAAd0lEQVQ4jb2VQRLAIAgDE///Z3qqY1FAhalHMCsCIkVEAIAkkVgvp2lDBgYAnAyHkWotLccNrEd4A7X2TqIdqLfnWBAdaF5rJdyJfjtPH5GT37CaGhoVq3nOm/XflUuLUto2pY1d+vRKh0Pp+MrAVtDe2JkvYNQ+jVSEEFmOkggAAAAASUVORK5CYII=") no-repeat center center;overflow:hidden;text-indent:-99999em;border:1px solid transparent}.at4-thankyou-background .at4lb-inner .at4x:focus,.at4-thankyou-background .at4lb-inner .at4x:hover{border:1px solid #fff;border-radius:50%;outline:0}.at4-thankyou-background .at4lb-inner #at4-palogo{position:absolute;bottom:10px;display:inline-block;text-decoration:none;font-family:helvetica,arial,sans-serif;font-size:11px;cursor:pointer;-webkit-transition:opacity .25s ease-in;moz-transition:opacity .25s ease-in;transition:opacity .25s ease-in;opacity:.5;z-index:100020;color:#fff;padding:2px 0 0 13px}.at4-thankyou-background .at4lb-inner #at4-palogo .at-branding-addthis,.at4-thankyou-background .at4lb-inner #at4-palogo .at-branding-info{color:#fff}.at4-thankyou-background .at4lb-inner #at4-palogo:hover,.at4-thankyou-background.ats-dark .at4lb-inner a#at4-palogo:hover{text-decoration:none;color:#fff;opacity:1}.at4-thankyou-background.ats-dark{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgeMZABGBiIBKMKqSOQoAAAwB+cQD6hqlbCwAAAABJRU5ErkJggg==");background:rgba(0,0,0,.85)}.at4-thankyou-background .thankyou-title{color:#fff;font-size:38.5px;margin:10px 20px;line-height:38.5px;font-family:helvetica neue,helvetica,arial,sans-serif;font-weight:300}.at4-thankyou-background.ats-dark .thankyou-description,.at4-thankyou-background.ats-dark .thankyou-title{color:#fff}.at4-thankyou-background .thankyou-description{color:#fff;font-size:18px;margin:10px 0;line-height:24px;padding:0;font-family:helvetica neue,helvetica,arial,sans-serif;font-weight:300}.at4-thankyou-background .at4-thanks-icons{padding-top:10px}.at4-thankyou-mobile *{-webkit-overflow-scrolling:touch}#at4-thankyou .at4-recommended-recommendedbox .at-logo{display:none}.at4-thankyou .at-h3{height:49px;line-height:49px;margin:0 50px 0 20px;padding:1px 0 0;font-family:helvetica neue,helvetica,arial,sans-serif;font-size:1pc;font-weight:700;color:#fff;text-shadow:0 1px #000}.at4-thanks{padding-top:50px;text-align:center}.at4-thanks label{display:block;margin:0 0 15px;font-size:1pc;line-height:1pc}.at4-thanks .at4-h2{background:none;border:none;margin:0 0 10px;padding:0;font-family:helvetica neue,helvetica,arial,sans-serif;font-size:28px;font-weight:300;color:#000}.at4-thanks .at4-thanks-icons{position:relative;height:2pc}.at4-thanks .at4-thanks-icons .at-thankyou-label{display:block;padding-bottom:10px;font-size:14px;color:#666}.at4-thankyou-layer .at-follow .at-icon-wrapper{width:2pc;height:2pc}</style><style type="text/css">.at4-recommended-toaster{position:fixed;top:auto;bottom:0;right:0;z-index:100021}.at4-recommended-toaster.ats-light{border:1px solid #c5c5c5;background:#fff}.at4-recommended-toaster.ats-gray{border:1px solid #c5c5c5;background:#f2f2f2}.at4-recommended-toaster.ats-dark{background:#262b30;color:#fff}.at4-recommended-toaster .at4-recommended-container{padding-top:0;margin:0}.at4-recommended.at4-recommended-toaster div.at-recommended-label{line-height:1pc;font-size:1pc;text-align:left;padding:20px 0 0 20px}.at4-toaster-outer .at4-recommended .at4-recommended-item .at4-recommended-item-caption .at-h4{font-size:11px;line-height:11px;margin:10px 0 6px;height:30px}.at4-recommended.at4-recommended-toaster div.at-recommended-label.ats-gray,.at4-recommended.at4-recommended-toaster div.at-recommended-label.ats-light{color:#464646}.at4-recommended.at4-recommended-toaster div.at-recommended-label.ats-dark{color:#fff}.at4-toaster-close-control{position:absolute;top:0;right:0;display:block;width:20px;height:20px;line-height:20px;margin:5px 5px 0 0;padding:0;text-indent:-9999em}.at4-toaster-open-control{position:fixed;right:0;bottom:0;z-index:100020}.at4-toaster-outer .at4-recommended-item{width:90pt;border:0;margin:9px 10px 0}.at4-toaster-outer .at4-recommended-item:first-child{margin-left:20px}.at4-toaster-outer .at4-recommended-item:last-child{margin-right:20px}.at4-toaster-outer .at4-recommended-item .at4-recommended-item-img{max-height:90pt;max-width:90pt}.at4-toaster-outer .at4-recommended-item .at4-recommended-item-img img{height:90pt;width:90pt}.at4-toaster-outer .at4-recommended-item .at4-recommended-item-caption{height:30px;padding:0;margin:0;height:initial}.at4-toaster-outer .ats-dark .at4-recommended-item .at4-recommended-item-caption{background:#262b30}.at4-toaster-outer .at4-recommended .at4-recommended-item .at4-recommended-item-caption small{width:auto;line-height:14px;margin:0}.at4-toaster-outer .at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption small{color:#fff}.at4-recommended-toaster .at-logo{margin:0 0 3px 20px;text-align:left}.at4-recommended-toaster .at-logo .at4-logo-container.at-sponsored-logo{position:relative}.at4-toaster-outer .at4-recommended-item .sponsored-label{text-align:right;font-size:10px;color:#666;float:right;position:fixed;bottom:6px;right:20px;top:initial;z-index:99999}</style><style type="text/css">.at4-whatsnext{position:fixed;bottom:0!important;right:0;background:#fff;border:1px solid #c5c5c5;margin:-1px;width:390px;height:90pt;overflow:hidden;font-size:9pt;font-weight:400;color:#000;z-index:1800000000}.at4-whatsnext a{color:#666}.at4-whatsnext .at-whatsnext-content{height:90pt;position:relative}.at4-whatsnext .at-whatsnext-content .at-branding{position:absolute;bottom:15px;right:10px;padding-left:9px;text-decoration:none;line-height:10px;font-family:helvetica,arial,sans-serif;font-size:10px;color:#666}.at4-whatsnext .at-whatsnext-content .at-whatsnext-content-inner{position:absolute;top:15px;right:20px;bottom:15px;left:140px;text-align:left;height:105px}.at4-whatsnext .at-whatsnext-content-inner a{display:inline-block}.at4-whatsnext .at-whatsnext-content-inner div.at-h6{text-align:left;margin:0;padding:0 0 3px;font-size:11px;color:#666;cursor:default}.at4-whatsnext .at-whatsnext-content .at-h3{text-align:left;margin:5px 0;padding:0;line-height:1.2em;font-weight:400;font-size:14px;height:3pc}.at4-whatsnext .at-whatsnext-content-inner a:link,.at4-whatsnext .at-whatsnext-content-inner a:visited{text-decoration:none;font-weight:400;color:#464646}.at4-whatsnext .at-whatsnext-content-inner a:hover{color:#000}.at4-whatsnext .at-whatsnext-content-inner small{position:absolute;bottom:15px;line-height:10px;font-size:11px;color:#666;cursor:default;text-align:left}.at4-whatsnext .at-whatsnext-content .at-whatsnext-content-img{position:absolute;top:0;left:0;width:90pt;height:90pt;overflow:hidden}.at4-whatsnext .at-whatsnext-content .at-whatsnext-content-img img{position:absolute;top:0;left:0;max-height:none;max-width:none}.at4-whatsnext .at-whatsnext-close-control{position:absolute;top:0;right:0;display:block;width:20px;height:20px;line-height:20px;margin:0 5px 0 0;padding:0;text-indent:-9999em}.at-whatsnext-open-control{position:fixed;right:0;bottom:0;z-index:100020}.at4-whatsnext.ats-dark{background:#262b30}.at4-whatsnext.ats-dark .at-whatsnext-content .at-h3,.at4-whatsnext.ats-dark .at-whatsnext-content a.at4-logo:hover,.at4-whatsnext.ats-dark .at-whatsnext-content-inner a:link,.at4-whatsnext.ats-dark .at-whatsnext-content-inner a:visited{color:#fff}.at4-whatsnext.ats-light{background:#fff}.at4-whatsnext.ats-gray{background:#f2f2f2}.at4-whatsnext.at-whatsnext-nophoto{width:270px}.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content-img{display:none}.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner{top:15px;right:0;left:20px}.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner.addthis_32x32_style{top:0;right:0;left:0;padding:45px 20px 0;font-size:20px}.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner .at4-icon,.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner .at4-icon-fw,.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner .whatsnext-msg{vertical-align:middle}.at-whatsnext-img,.at-whatsnext-img-lnk{position:absolute;left:0}</style><style type="text/css">.at4-whatsnextmobile{position:fixed;bottom:0;right:0;left:0;background:#fff;z-index:9999998;height:170px;font-size:28px}.at4-whatsnextmobile .col-2{height:100%;font-size:1em}.at4-whatsnextmobile .col-2:first-child{max-width:200px;display:inline-block;float:left}.at4-whatsnextmobile .col-2:last-child{position:absolute;left:200px;right:50px;top:0;bottom:0;display:inline-block}.at4-whatsnextmobile .at-whatsnext-content-inner{font-size:1em}.at4-whatsnextmobile .at-whatsnext-content-img img{height:100%;width:100%}.at4-whatsnextmobile .at-close-control{font-size:1em;position:absolute;top:0;right:0;width:50px;height:50px}.at4-whatsnextmobile .at-close-control button{width:100%;height:100%;font-size:1em;font-weight:400;text-decoration:none;opacity:.5;padding:0;cursor:pointer;background:0 0;border:0;-webkit-appearance:none}.at4-whatsnextmobile .at-h3,.at4-whatsnextmobile .at-h6{font-size:1em;margin:0;color:#a1a1a1;margin-left:2.5%;margin-top:25px}.at4-whatsnextmobile .at-h3{font-size:1em;line-height:1em;font-weight:500;height:50%}.at4-whatsnextmobile .at-h3 a{font-size:1em;text-decoration:none}.at4-whatsnextmobile .at-h6{font-size:.8em;line-height:.8em;font-weight:500}.at4-whatsnextmobile .footer{position:absolute;bottom:2px;left:200px;right:0;padding-left:2.5%;font-size:1em;line-height:.6em}.at4-whatsnextmobile .footer small{font-size:.6em;color:#a1a1a1}.at4-whatsnextmobile .footer small:first-child{margin-right:5%;float:left}.at4-whatsnextmobile .footer small:last-child{margin-right:2.5%;float:right}.at4-whatsnextmobile .at-whatsnext-content{height:100%}.at4-whatsnextmobile.ats-dark{background:#262b30;color:#fff}.at4-whatsnextmobile .at-close-control button{color:#bfbfbf}.at4-whatsnextmobile.ats-dark a:link,.at4-whatsnextmobile.ats-dark a:visited{color:#fff}.at4-whatsnextmobile.ats-gray{background:#f2f2f2;color:#262b30}.at4-whatsnextmobile.ats-light{background:#fff;color:#262b30}.at4-whatsnextmobile.ats-dark .footer a:link,.at4-whatsnextmobile.ats-dark .footer a:visited,.at4-whatsnextmobile.ats-gray .footer a:link,.at4-whatsnextmobile.ats-gray .footer a:visited,.at4-whatsnextmobile.ats-light .footer a:link,.at4-whatsnextmobile.ats-light .footer a:visited{color:#a1a1a1}.at4-whatsnextmobile.ats-gray a:link,.at4-whatsnextmobile.ats-gray a:visited,.at4-whatsnextmobile.ats-light a:link,.at4-whatsnextmobile.ats-light a:visited{color:#262b30}@media only screen and (min-device-width:320px) and (max-device-width:480px){.at4-whatsnextmobile{height:85px;font-size:14px}.at4-whatsnextmobile .col-2:first-child{width:75pt}.at4-whatsnextmobile .col-2:last-child{right:25px;left:75pt}.at4-whatsnextmobile .footer{left:75pt}.at4-whatsnextmobile .at-close-control{width:25px;height:25px}.at4-whatsnextmobile .at-h3,.at4-whatsnextmobile .at-h6{margin-top:12.5px}}</style><style type="text/css">.at-custom-mobile-bar{left:0;right:0;width:100%;height:56px;position:fixed;text-align:center;z-index:100020;background:#fff;overflow:hidden;box-shadow:0 0 10px 0 rgba(0,0,0,.2);font:initial;line-height:normal;top:auto;bottom:0}.at-custom-mobile-bar.at-custom-mobile-bar-zindex-hide{z-index:-1!important}.at-custom-mobile-bar.atss-top{top:0;bottom:auto}.at-custom-mobile-bar.atss-bottom{top:auto;bottom:0}.at-custom-mobile-bar .at-custom-mobile-bar-btns{display:inline-block;text-align:center}.at-custom-mobile-bar .at-custom-mobile-bar-counter,.at-custom-mobile-bar .at-share-btn{margin-top:4px}.at-custom-mobile-bar .at-share-btn{display:inline-block;text-decoration:none;transition:none;box-sizing:content-box}.at-custom-mobile-bar .at-custom-mobile-bar-counter{font-family:Helvetica neue,arial;vertical-align:top;margin-left:4px;margin-right:4px;display:inline-block}.at-custom-mobile-bar .at-custom-mobile-bar-count{font-size:26px;line-height:1.25em;color:#222}.at-custom-mobile-bar .at-custom-mobile-bar-text{font-size:9pt;line-height:1.25em;color:#888;letter-spacing:1px}.at-custom-mobile-bar .at-icon-wrapper{text-align:center;height:3pc;width:3pc;margin:0 4px}.at-custom-mobile-bar .at-icon{vertical-align:top;margin:8px;width:2pc;height:2pc}.at-custom-mobile-bar.at-shfs-medium{height:3pc}.at-custom-mobile-bar.at-shfs-medium .at-custom-mobile-bar-counter{margin-top:6px}.at-custom-mobile-bar.at-shfs-medium .at-custom-mobile-bar-count{font-size:18px}.at-custom-mobile-bar.at-shfs-medium .at-custom-mobile-bar-text{font-size:10px}.at-custom-mobile-bar.at-shfs-medium .at-icon-wrapper{height:40px;width:40px}.at-custom-mobile-bar.at-shfs-medium .at-icon{margin:6px;width:28px;height:28px}.at-custom-mobile-bar.at-shfs-small{height:40px}.at-custom-mobile-bar.at-shfs-small .at-custom-mobile-bar-counter{margin-top:3px}.at-custom-mobile-bar.at-shfs-small .at-custom-mobile-bar-count{font-size:1pc}.at-custom-mobile-bar.at-shfs-small .at-custom-mobile-bar-text{font-size:10px}.at-custom-mobile-bar.at-shfs-small .at-icon-wrapper{height:2pc;width:2pc}.at-custom-mobile-bar.at-shfs-small .at-icon{margin:4px;width:24px;height:24px}</style><style type="text/css">.at-custom-sidebar{top:20%;width:58px;position:fixed;text-align:center;z-index:100020;background:#fff;overflow:hidden;box-shadow:0 0 10px 0 rgba(0,0,0,.2);font:initial;line-height:normal;top:auto;bottom:0}.at-custom-sidebar.at-custom-sidebar-zindex-hide{z-index:-1!important}.at-custom-sidebar.atss-left{left:0;right:auto;float:left;border-radius:0 4px 4px 0}.at-custom-sidebar.atss-right{left:auto;right:0;float:right;border-radius:4px 0 0 4px}.at-custom-sidebar .at-custom-sidebar-btns{display:inline-block;text-align:center;padding-top:4px}.at-custom-sidebar .at-custom-sidebar-counter{margin-bottom:8px}.at-custom-sidebar .at-share-btn{display:inline-block;text-decoration:none;transition:none;box-sizing:content-box}.at-custom-sidebar .at-custom-sidebar-counter{font-family:Helvetica neue,arial;vertical-align:top;margin-left:4px;margin-right:4px;display:inline-block}.at-custom-sidebar .at-custom-sidebar-count{font-size:21px;line-height:1.25em;color:#222}.at-custom-sidebar .at-custom-sidebar-text{font-size:10px;line-height:1.25em;color:#888;letter-spacing:1px}.at-custom-sidebar .at-icon-wrapper{text-align:center;margin:0 4px}.at-custom-sidebar .at-icon{vertical-align:top;margin:9px;width:2pc;height:2pc}.at-custom-sidebar .at-icon-wrapper{position:relative}.at-custom-sidebar .at4-share-count,.at-custom-sidebar .at4-share-count-container{line-height:1pc;font-size:10px}.at-custom-sidebar .at4-share-count{text-indent:0;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-weight:200;width:100%;height:1pc}.at-custom-sidebar .at4-share-count-anchor .at-icon{margin-top:3px}.at-custom-sidebar .at4-share-count-container{position:absolute;left:0;right:auto;top:auto;bottom:0;width:100%;color:#fff;background:inherit}</style><style type="text/css">.at-image-sharing-mobile-icon{position:absolute;background:#000 url(//s7.addthis.com/static/44a36d35bafef33aa9455b7d3039a771.png) no-repeat top center;background-color:rgba(0,0,0,.9);background-image:url(//s7.addthis.com/static/10db525181ee0bbe1a515001be1c7818.svg),none;border-radius:3px;width:50px;height:40px;top:-9999px;left:-9999px}.at-image-sharing-tool{display:block;position:absolute;text-align:center;z-index:9001;background:none;overflow:hidden;top:-9999px;left:-9999px;font:initial;line-height:0}.at-image-sharing-tool.addthis-animated{animation-duration:.15s}.at-image-sharing-tool.at-orientation-vertical .at-share-btn{display:block}.at-image-sharing-tool.at-orientation-horizontal .at-share-btn{display:inline-block}.at-image-sharing-tool.at-image-sharing-tool-size-big .at-icon{width:43px;height:43px}.at-image-sharing-tool.at-image-sharing-tool-size-mobile .at-share-btn{margin:0!important}.at-image-sharing-tool.at-image-sharing-tool-size-mobile .at-icon-wrapper{height:60px;width:100%;border-radius:0!important}.at-image-sharing-tool.at-image-sharing-tool-size-mobile .at-icon{max-width:100%;height:54px!important;width:54px!important}.at-image-sharing-tool .at-custom-shape.at-image-sharing-tool-btns{margin-right:8px;margin-bottom:8px}.at-image-sharing-tool .at-custom-shape .at-share-btn{margin-top:8px;margin-left:8px}.at-image-sharing-tool .at-share-btn{line-height:0;text-decoration:none;transition:none;box-sizing:content-box}.at-image-sharing-tool .at-icon-wrapper{text-align:center;height:100%;width:100%}.at-image-sharing-tool .at-icon{vertical-align:top;width:2pc;height:2pc;margin:3px}</style><style type="text/css">.at-expanding-share-button{box-sizing:border-box;position:fixed;z-index:9999}.at-expanding-share-button[data-position=bottom-right]{bottom:10px;right:10px}.at-expanding-share-button[data-position=bottom-right] .at-expanding-share-button-toggle-bg,.at-expanding-share-button[data-position=bottom-right] .at-expanding-share-button-toggle-btn[data-name]:after,.at-expanding-share-button[data-position=bottom-right] .at-icon-wrapper,.at-expanding-share-button[data-position=bottom-right] [data-name]:after{float:right}.at-expanding-share-button[data-position=bottom-right] [data-name]:after{margin-right:10px}.at-expanding-share-button[data-position=bottom-right] .at-expanding-share-button-toggle-btn[data-name]:after{margin-right:5px}.at-expanding-share-button[data-position=bottom-right] .at-icon-wrapper{margin-right:-3px}.at-expanding-share-button[data-position=bottom-left]{bottom:10px;left:10px}.at-expanding-share-button[data-position=bottom-left] .at-expanding-share-button-toggle-bg,.at-expanding-share-button[data-position=bottom-left] .at-expanding-share-button-toggle-btn[data-name]:after,.at-expanding-share-button[data-position=bottom-left] .at-icon-wrapper,.at-expanding-share-button[data-position=bottom-left] [data-name]:after{float:left}.at-expanding-share-button[data-position=bottom-left] [data-name]:after{margin-left:10px}.at-expanding-share-button[data-position=bottom-left] .at-expanding-share-button-toggle-btn[data-name]:after{margin-left:5px}.at-expanding-share-button *,.at-expanding-share-button :after,.at-expanding-share-button :before{box-sizing:border-box}.at-expanding-share-button .at-expanding-share-button-services-list{display:none;list-style:none;margin:0 5px;overflow:visible;padding:0}.at-expanding-share-button .at-expanding-share-button-services-list>li{display:block;height:45px;position:relative;overflow:visible}.at-expanding-share-button .at-expanding-share-button-toggle-btn,.at-expanding-share-button .at-share-btn{transition:.1s;text-decoration:none}.at-expanding-share-button .at-share-btn{display:block;height:40px;padding:0 3px 0 0}.at-expanding-share-button .at-expanding-share-button-toggle-btn{position:relative;overflow:auto}.at-expanding-share-button .at-expanding-share-button-toggle-btn.at-expanding-share-button-hidden[data-name]:after{display:none}.at-expanding-share-button .at-expanding-share-button-toggle-bg{box-shadow:0 2px 4px 0 rgba(0,0,0,.3);border-radius:50%;position:relative}.at-expanding-share-button .at-expanding-share-button-toggle-bg>span{background-image:url("data:image/svg+xml,%3Csvg%20width%3D%2232px%22%20height%3D%2232px%22%20viewBox%3D%220%200%2032%2032%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Ctitle%3Eshare%3C%2Ftitle%3E%3Cg%20stroke%3D%22none%22%20stroke-width%3D%221%22%20fill%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%3Cg%20fill%3D%22%23FFFFFF%22%3E%3Cpath%20d%3D%22M26%2C13.4285714%20C26%2C13.6220248%2025.9293162%2C13.7894338%2025.7879464%2C13.9308036%20L20.0736607%2C19.6450893%20C19.932291%2C19.786459%2019.7648819%2C19.8571429%2019.5714286%2C19.8571429%20C19.3779752%2C19.8571429%2019.2105662%2C19.786459%2019.0691964%2C19.6450893%20C18.9278267%2C19.5037195%2018.8571429%2C19.3363105%2018.8571429%2C19.1428571%20L18.8571429%2C16.2857143%20L16.3571429%2C16.2857143%20C15.6279725%2C16.2857143%2014.9750773%2C16.3080355%2014.3984375%2C16.3526786%20C13.8217977%2C16.3973217%2013.2488868%2C16.477306%2012.6796875%2C16.5926339%20C12.1104882%2C16.7079619%2011.6157015%2C16.8660704%2011.1953125%2C17.0669643%20C10.7749235%2C17.2678581%2010.3824423%2C17.5264121%2010.0178571%2C17.8426339%20C9.65327199%2C18.1588557%209.35565592%2C18.534596%209.125%2C18.9698661%20C8.89434408%2C19.4051361%208.71391434%2C19.9203839%208.58370536%2C20.515625%20C8.45349637%2C21.1108661%208.38839286%2C21.7842224%208.38839286%2C22.5357143%20C8.38839286%2C22.9449425%208.40699386%2C23.4025272%208.44419643%2C23.9084821%20C8.44419643%2C23.9531252%208.45349693%2C24.0405499%208.47209821%2C24.1707589%20C8.4906995%2C24.3009679%208.5%2C24.3995532%208.5%2C24.4665179%20C8.5%2C24.5781256%208.46837829%2C24.6711306%208.40513393%2C24.7455357%20C8.34188956%2C24.8199408%208.25446484%2C24.8571429%208.14285714%2C24.8571429%20C8.02380893%2C24.8571429%207.9196433%2C24.7938994%207.83035714%2C24.6674107%20C7.77827355%2C24.6004461%207.72991094%2C24.5186017%207.68526786%2C24.421875%20C7.64062478%2C24.3251483%207.59040206%2C24.2135423%207.53459821%2C24.0870536%20C7.47879436%2C23.9605648%207.43973225%2C23.87128%207.41741071%2C23.8191964%20C6.47246551%2C21.6986501%206%2C20.0208395%206%2C18.7857143%20C6%2C17.3050521%206.19717065%2C16.0662252%206.59151786%2C15.0691964%20C7.79688103%2C12.0706695%2011.0520568%2C10.5714286%2016.3571429%2C10.5714286%20L18.8571429%2C10.5714286%20L18.8571429%2C7.71428571%20C18.8571429%2C7.52083237%2018.9278267%2C7.35342333%2019.0691964%2C7.21205357%20C19.2105662%2C7.07068382%2019.3779752%2C7%2019.5714286%2C7%20C19.7648819%2C7%2019.932291%2C7.07068382%2020.0736607%2C7.21205357%20L25.7879464%2C12.9263393%20C25.9293162%2C13.067709%2026%2C13.2351181%2026%2C13.4285714%20L26%2C13.4285714%20Z%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E");background-position:center center;background-repeat:no-repeat;transition:transform .4s ease;border-radius:50%;display:block}.at-expanding-share-button .at-icon-wrapper{box-shadow:0 2px 4px 0 rgba(0,0,0,.3);border-radius:50%;display:inline-block;height:40px;line-height:40px;text-align:center;width:40px}.at-expanding-share-button .at-icon{display:inline-block;height:34px;margin:3px 0;vertical-align:top;width:34px}.at-expanding-share-button [data-name]:after{box-shadow:0 2px 4px 0 rgba(0,0,0,.3);transform:translate(0, -50%);transition:.4s;background-color:#fff;border-radius:3px;color:#666;content:attr(data-name);font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:9pt;line-height:9pt;font-weight:500;opacity:0;padding:3px 5px;position:relative;top:20px;white-space:nowrap}.at-expanding-share-button.at-expanding-share-button-show-icons .at-expanding-share-button-services-list{display:block}.at-expanding-share-button.at-expanding-share-button-animate-in .at-expanding-share-button-toggle-bg>span{transform:rotate(270deg);background-image:url("data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20viewBox%3D%220%200%2032%2032%22%3E%3Cg%3E%3Cpath%20d%3D%22M18%2014V8h-4v6H8v4h6v6h4v-6h6v-4h-6z%22%20fill-rule%3D%22evenodd%22%20fill%3D%22white%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3C%2Fsvg%3E");background-position:center center;background-repeat:no-repeat}.at-expanding-share-button.at-expanding-share-button-animate-in [data-name]:after{opacity:1}.at-expanding-share-button.at-hide-label [data-name]:after{display:none}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle{height:50px}.at-expanding-share-button.at-expanding-share-button-desktop .at-icon-wrapper:hover{box-shadow:0 2px 5px 0 rgba(0,0,0,.5)}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-bg{height:50px;line-height:50px;width:50px}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-bg>span{height:50px;width:50px}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-bg:after{box-shadow:0 2px 5px 0 rgba(0,0,0,.2);transition:opacity .2s ease;border-radius:50%;content:'';height:100%;opacity:0;position:absolute;top:0;left:0;width:100%}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-bg:hover:after{opacity:1}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-btn[data-name]:after{top:25px}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-services-list{margin:0}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-toggle-btn,.at-expanding-share-button.at-expanding-share-button-mobile .at-share-btn{outline:0}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-toggle{height:40px;-webkit-tap-highlight-color:transparent}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-toggle-bg,.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-toggle-bg span{height:40px;line-height:40px;width:40px}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-click-flash{transform:scale(0);transition:transform ease,opacity ease-in;background-color:hsla(0,0%,100%,.3);border-radius:50%;height:40px;opacity:1;position:absolute;width:40px;z-index:10000}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-click-flash.at-expanding-share-button-click-flash-animate{transform:scale(1);opacity:0}.at-expanding-share-button.at-expanding-share-button-mobile+.at-expanding-share-button-mobile-overlay{transition:opacity ease;bottom:0;background-color:hsla(0,0%,87%,.7);display:block;height:auto;left:0;opacity:0;position:fixed;right:0;top:0;width:auto;z-index:9998}.at-expanding-share-button.at-expanding-share-button-mobile+.at-expanding-share-button-mobile-overlay.at-expanding-share-button-hidden{height:0;width:0;z-index:-10000}.at-expanding-share-button.at-expanding-share-button-mobile.at-expanding-share-button-animate-in+.at-expanding-share-button-mobile-overlay{transition:opacity ease;opacity:1}</style><style type="text/css">.at-tjin-element .at300b,.at-tjin-element .at300m{display:inline-block;width:auto;padding:0;margin:0 2px 5px;outline-offset:-1px;transition:all .2s ease-in-out}.at-tjin-element .at300b:focus,.at-tjin-element .at300b:hover,.at-tjin-element .at300m:focus,.at-tjin-element .at300m:hover{transform:translateY(-4px)}.at-tjin-element .addthis_tjin_label{display:none}.at-tjin-element .addthis_vertical_style .at300b,.at-tjin-element .addthis_vertical_style .at300m{display:block}.at-tjin-element .addthis_vertical_style .at300b .addthis_tjin_label,.at-tjin-element .addthis_vertical_style .at300b .at-icon-wrapper,.at-tjin-element .addthis_vertical_style .at300m .addthis_tjin_label,.at-tjin-element .addthis_vertical_style .at300m .at-icon-wrapper{display:inline-block;vertical-align:middle;margin-right:5px}.at-tjin-element .addthis_vertical_style .at300b:focus,.at-tjin-element .addthis_vertical_style .at300b:hover,.at-tjin-element .addthis_vertical_style .at300m:focus,.at-tjin-element .addthis_vertical_style .at300m:hover{transform:none}.at-tjin-element .at-tjin-btn{margin:0 5px 5px 0;padding:0;outline-offset:-1px;display:inline-block;box-sizing:content-box;transition:all .2s ease-in-out}.at-tjin-element .at-tjin-btn:focus,.at-tjin-element .at-tjin-btn:hover{transform:translateY(-4px)}.at-tjin-element .at-tjin-title{margin:0 0 15px}</style><style type="text/css">#addthissmartlayerscssready{color:#bada55!important}.addthis-smartlayers,div#at4-follow,div#at4-share,div#at4-thankyou,div#at4-whatsnext{padding:0;margin:0}#at4-follow-label,#at4-share-label,#at4-whatsnext-label,.at4-recommended-label.hidden{padding:0;border:none;background:none;position:absolute;top:0;left:0;height:0;width:0;overflow:hidden;text-indent:-9999em}.addthis-smartlayers .at4-arrow:hover{cursor:pointer}.addthis-smartlayers .at4-arrow:after,.addthis-smartlayers .at4-arrow:before{content:none}a.at4-logo{background:url(data:image/gif;base64,R0lGODlhBwAHAJEAAP9uQf///wAAAAAAACH5BAkKAAIALAAAAAAHAAcAAAILFH6Ge8EBH2MKiQIAOw==) no-repeat left center}.at4-minimal a.at4-logo{background:url(data:image/gif;base64,R0lGODlhBwAHAJEAAP9uQf///wAAAAAAACH5BAkKAAIALAAAAAAHAAcAAAILFH6Ge8EBH2MKiQIAOw==) no-repeat left center!important}button.at4-closebutton{position:absolute;top:0;right:0;padding:0;margin-right:10px;cursor:pointer;background:transparent;border:0;-webkit-appearance:none;font-size:19px;line-height:1;color:#000;text-shadow:0 1px 0 #fff;opacity:.2}button.at4-closebutton:hover{color:#000;text-decoration:none;cursor:pointer;opacity:.5}div.at4-arrow{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAoCAYAAABpYH0BAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAV1JREFUeNrsmesOgyAMhQfxwfrofTM3E10ME2i5Oeppwr9a5OMUCrh1XV+wcvNAAIAA+BiAzrmtUWln27dbjEcC3AdODfo0BdEPhmcO4nIDvDNELi2jggk4/k8dT7skfeKzWIEd4VUpMQKvNB7X+OZSmAZkATWC1xvipbpnLmOosbJZC08CkAeA4E6qFUEMwLAGnlSBPCE8lW8CYnZTcimH2HoT7kSFOx5HBmCnDhTIu1p5s98G+QZrxGPhZVMY1vgyAQaAAAiAAAgDQACcBOD+BvJtBWfRy7NpJK5tBe4FNzXokywV734wPHMQlxvgnSGyNoUP/2ACjv/7iSeYKO3YWKzAjvCqlBiBVxqPa3ynexNJwOsN8TJbzL6JNIYYXWpMv4lIIAZgWANPqkCeEJ7KNwExu8lpLlSpAVQarO77TyKdBsyRPuwV0h0gmoGnTWFYzVkYBoAA+I/2FmAAt6+b5XM9mFkAAAAASUVORK5CYII=);background-repeat:no-repeat;width:20px;height:20px;margin:0;padding:0;overflow:hidden;text-indent:-9999em;text-align:left;cursor:pointer}#at4-recommendedpanel-outer-container .at4-arrow.at-right,div.at4-arrow.at-right{background-position:-20px 0}#at4-recommendedpanel-outer-container .at4-arrow.at-left,div.at4-arrow.at-left{background-position:0 0}div.at4-arrow.at-down{background-position:-60px 0}div.at4-arrow.at-up{background-position:-40px 0}.ats-dark div.at4-arrow.at-right{background-position:-20px -20px}.ats-dark div.at4-arrow.at-left{background-position:0 -20px}.ats-dark div.at4-arrow.at-down{background-position:-60px -20px}.ats-dark div.at4-arrow.at-up{background-position:-40px -20}.at4-opacity-hidden{opacity:0!important}.at4-opacity-visible{opacity:1!important}.at4-visually-hidden{position:absolute;clip:rect(1px,1px,1px,1px);padding:0;border:0;overflow:hidden}.at4-hidden-off-screen,.at4-hidden-off-screen *{position:absolute!important;top:-9999px!important;left:-9999px!important}.at4-show{display:block!important;opacity:1!important}.at4-show-content{opacity:1!important;visibility:visible}.at4-hide{display:none!important;opacity:0!important}.at4-hide-content{opacity:0!important;visibility:hidden}.at4-visible{display:block!important;opacity:0!important}.at-wordpress-hide{display:none!important;opacity:0!important}.addthis-animated{animation-fill-mode:both;animation-timing-function:ease-out;animation-duration:.3s}.slideInDown.addthis-animated,.slideInLeft.addthis-animated,.slideInRight.addthis-animated,.slideInUp.addthis-animated,.slideOutDown.addthis-animated,.slideOutLeft.addthis-animated,.slideOutRight.addthis-animated,.slideOutUp.addthis-animated{animation-duration:.4s}@keyframes fadeIn{0%{opacity:0}to{opacity:1}}.fadeIn{animation-name:fadeIn}@keyframes fadeInUp{0%{opacity:0;transform:translateY(20px)}to{opacity:1;transform:translateY(0)}}.fadeInUp{animation-name:fadeInUp}@keyframes fadeInDown{0%{opacity:0;transform:translateY(-20px)}to{opacity:1;transform:translateY(0)}}.fadeInDown{animation-name:fadeInDown}@keyframes fadeInLeft{0%{opacity:0;transform:translateX(-20px)}to{opacity:1;transform:translateX(0)}}.fadeInLeft{animation-name:fadeInLeft}@keyframes fadeInRight{0%{opacity:0;transform:translateX(20px)}to{opacity:1;transform:translateX(0)}}.fadeInRight{animation-name:fadeInRight}@keyframes fadeOut{0%{opacity:1}to{opacity:0}}.fadeOut{animation-name:fadeOut}@keyframes fadeOutUp{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(-20px)}}.fadeOutUp{animation-name:fadeOutUp}@keyframes fadeOutDown{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(20px)}}.fadeOutDown{animation-name:fadeOutDown}@keyframes fadeOutLeft{0%{opacity:1;transform:translateX(0)}to{opacity:0;transform:translateX(-20px)}}.fadeOutLeft{animation-name:fadeOutLeft}@keyframes fadeOutRight{0%{opacity:1;transform:translateX(0)}to{opacity:0;transform:translateX(20px)}}.fadeOutRight{animation-name:fadeOutRight}@keyframes slideInUp{0%{transform:translateY(1500px)}0%,to{opacity:1}to{transform:translateY(0)}}.slideInUp{animation-name:slideInUp}.slideInUp.addthis-animated{animation-duration:.4s}@keyframes slideInDown{0%{transform:translateY(-850px)}0%,to{opacity:1}to{transform:translateY(0)}}.slideInDown{animation-name:slideInDown}@keyframes slideOutUp{0%{transform:translateY(0)}0%,to{opacity:1}to{transform:translateY(-250px)}}.slideOutUp{animation-name:slideOutUp}@keyframes slideOutUpFast{0%{transform:translateY(0)}0%,to{opacity:1}to{transform:translateY(-1250px)}}#at4m-menu.slideOutUp{animation-name:slideOutUpFast}@keyframes slideOutDown{0%{transform:translateY(0)}0%,to{opacity:1}to{transform:translateY(350px)}}.slideOutDown{animation-name:slideOutDown}@keyframes slideOutDownFast{0%{transform:translateY(0)}0%,to{opacity:1}to{transform:translateY(1250px)}}#at4m-menu.slideOutDown{animation-name:slideOutDownFast}@keyframes slideInLeft{0%{opacity:0;transform:translateX(-850px)}to{transform:translateX(0)}}.slideInLeft{animation-name:slideInLeft}@keyframes slideInRight{0%{opacity:0;transform:translateX(1250px)}to{transform:translateX(0)}}.slideInRight{animation-name:slideInRight}@keyframes slideOutLeft{0%{transform:translateX(0)}to{opacity:0;transform:translateX(-350px)}}.slideOutLeft{animation-name:slideOutLeft}@keyframes slideOutRight{0%{transform:translateX(0)}to{opacity:0;transform:translateX(350px)}}.slideOutRight{animation-name:slideOutRight}.at4win{margin:0 auto;background:#fff;border:1px solid #ebeced;width:25pc;box-shadow:0 0 10px rgba(0,0,0,.3);border-radius:8px;font-family:helvetica neue,helvetica,arial,sans-serif;text-align:left;z-index:9999}.at4win .at4win-header{position:relative;border-bottom:1px solid #f2f2f2;background:#fff;height:49px;-webkit-border-top-left-radius:8px;-webkit-border-top-right-radius:8px;-moz-border-radius-topleft:8px;-moz-border-radius-topright:8px;border-top-left-radius:8px;border-top-right-radius:8px;cursor:default}.at4win .at4win-header .at-h3,.at4win .at4win-header h3{height:49px;line-height:49px;margin:0 50px 0 0;padding:1px 0 0;margin-left:20px;font-family:helvetica neue,helvetica,arial,sans-serif;font-size:1pc;font-weight:700;text-shadow:0 1px #fff;color:#333}.at4win .at4win-header .at-h3 img,.at4win .at4win-header h3 img{display:inline-block;margin-right:4px}.at4win .at4win-header .at4-close{display:block;position:absolute;top:0;right:0;background:url("data:image/gif;base64,R0lGODlhFAAUAIABAAAAAP///yH5BAEAAAEALAAAAAAUABQAAAIzBIKpG+YMm5Enpodw1HlCfnkKOIqU1VXk55goVb2hi7Y0q95lfG70uurNaqLgTviyyUoFADs=") no-repeat center center;background-repeat:no-repeat;background-position:center center;border-left:1px solid #d2d2d1;width:49px;height:49px;line-height:49px;overflow:hidden;text-indent:-9999px;text-shadow:none;cursor:pointer;opacity:.5;border:0;transition:opacity .15s ease-in}.at4win .at4win-header .at4-close::-moz-focus-inner{border:0;padding:0}.at4win .at4win-header .at4-close:hover{opacity:1;background-color:#ebeced;border-top-right-radius:7px}.at4win .at4win-content{position:relative;background:#fff;min-height:220px}#at4win-footer{position:relative;background:#fff;border-top:1px solid #d2d2d1;-webkit-border-bottom-right-radius:8px;-webkit-border-bottom-left-radius:8px;-moz-border-radius-bottomright:8px;-moz-border-radius-bottomleft:8px;border-bottom-right-radius:8px;border-bottom-left-radius:8px;height:11px;line-height:11px;padding:5px 20px;font-size:11px;color:#666;-ms-box-sizing:content-box;-o-box-sizing:content-box;box-sizing:content-box}#at4win-footer a{margin-right:10px;text-decoration:none;color:#666}#at4win-footer a:hover{text-decoration:none;color:#000}#at4win-footer a.at4-logo{top:5px;padding-left:10px}#at4win-footer a.at4-privacy{position:absolute;top:5px;right:10px;padding-right:14px}.at4win.ats-dark{border-color:#555;box-shadow:none}.at4win.ats-dark .at4win-header{background:#1b1b1b;-webkit-border-top-left-radius:6px;-webkit-border-top-right-radius:6px;-moz-border-radius-topleft:6px;-moz-border-radius-topright:6px;border-top-left-radius:6px;border-top-right-radius:6px}.at4win.ats-dark .at4win-header .at4-close{background:url("data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAAAAWdEVYdENyZWF0aW9uIFRpbWUAMTEvMTMvMTKswDp5AAAAd0lEQVQ4jb2VQRLAIAgDE///Z3qqY1FAhalHMCsCIkVEAIAkkVgvp2lDBgYAnAyHkWotLccNrEd4A7X2TqIdqLfnWBAdaF5rJdyJfjtPH5GT37CaGhoVq3nOm/XflUuLUto2pY1d+vRKh0Pp+MrAVtDe2JkvYNQ+jVSEEFmOkggAAAAASUVORK5CYII=") no-repeat center center;background-image:url(//s7.addthis.com/static/fb08f6d50887bd0caacc86a62bcdcf68.svg),none;border-color:#333}.at4win.ats-dark .at4win-header .at4-close:hover{background-color:#000}.at4win.ats-dark .at4win-header .at-h3,.at4win.ats-dark .at4win-header h3{color:#fff;text-shadow:0 1px #000}.at4win.ats-gray .at4win-header{background:#fff;border-color:#d2d2d1;-webkit-border-top-left-radius:6px;-webkit-border-top-right-radius:6px;-moz-border-radius-topleft:6px;-moz-border-radius-topright:6px;border-top-left-radius:6px;border-top-right-radius:6px}.at4win.ats-gray .at4win-header a.at4-close{border-color:#d2d2d1}.at4win.ats-gray .at4win-header a.at4-close:hover{background-color:#ebeced}.at4win.ats-gray #at4win-footer{border-color:#ebeced}.at4win .clear{clear:both}.at4win ::selection{background:#fe6d4c;color:#fff}.at4win ::-moz-selection{background:#fe6d4c;color:#fff}.at4-icon-fw{display:inline-block;background-repeat:no-repeat;background-position:0 0;margin:0 5px 0 0;overflow:hidden;text-indent:-9999em;cursor:pointer;padding:0;border-radius:50%;-moz-border-radius:50%;-webkit-border-radius:50%}.at44-follow-container a.aticon{height:2pc;margin:0 5px 5px 0}.at44-follow-container .at4-icon-fw{margin:0}</style><script type="text/javascript" charset="UTF-8" src="./Certy_files/vt"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/controls.js"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/AuthenticationService.Authenticate"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/marker.js"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/QuotaService.RecordEvent"></script><script type="text/javascript" charset="UTF-8" src="./Certy_files/vt(1)"></script></head>

  <body class="" style="margin-right: 0px; margin-bottom: 0px;">
<style>
.crt-paper-layers{
     page-break-before: always; 
     }
</style>

     <div class="crt-wrapper">
         <header id="crt-header">
         </header><!-- #crt-header -->
        
                              

        <div id="crt-container" class="crt-container"  style="margin-top: 50px;">
            <div id="crt-side-box-wrap" class="crt-sticky">
    <div id="crt-side-box">

    <div class="crt-side-box-item">
        
<div class="crt-card bg-primary text-center">
    <div class="crt-card-avatar">
        <img class="avatar avatar-195" src="img/profile.jpg" width="195" height="195" alt="">
    </div>
    <div class="crt-card-info">
        <h2 class="text-upper">Ezequiel Paolillo</h2>

        <p class="">Consultor en Tecnología</p>
        <p class="">-</p>
        <p class="">Especialista en soluciones BI Open Source</p>
        <ul class="crt-social clear-list">
            <li><a><span class=""></span></a></li>
            <li><a href="https://github.com/epaolillo"><i class="fab fa-github"></i></a></li>
            <li><a href="https://gitlab.com/ezequielp"><i class="fab fa-gitlab"></i></a></li>
            <li><a href="https://www.linkedin.com/in/ezequiel-paolillo/"><i class="fab fa-linkedin-in"></i></a></li>
        </ul>
    </div>
</div>
    </div><!-- .crt-side-box-item -->

    </div><!-- #crt-side-box -->
</div><!-- #crt-side-box-wrap -->

            
            <div class="crt-container-sm">        <div id="about" class="crt-paper-layers crt-animated">
            <div class="crt-paper clearfix">
                <div class="crt-paper-cont paper-padd clear-mrg">

                    <section class="section brd-btm padd-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="title-lg text-upper">¡Hola!, soy Ezequiel</h2>

                                <div class="text-box">
                                    <p><b></b><br>
                                    Soy un entusiasta de la tecnología, me encanta entregar soluciones innovadoras en cada proyecto. Busco soluciones creativas constantemente, creo que existe un conjunto de metodologías, estándares y lenguajes acorde a cada proyecto. Me especializo en soluciones de BI Open Source (SQL, noSQL, Visualizaciones, reporting) y complemento este perfil con muy sólidos conocimientos en desarrollo (Java, PHP, NodeJS). Ya sean sistemas de gestión de recursos humanos, simulación, optimización, o desarrollos tecnológicos de cualquier índole, puedo hacerlo.</p>
                                </div>
                            </div>
                        </div>
                        <!-- .row -->

                        <div class="row">
                            <div class="col-sm-9">
                                <div class="crt-share-box clearfix">
                                </div>
                                <!-- .crt-share -->
                            </div>
                            <div class="col-sm-3 text-right">
                                <img src="img/Selection_602.png" alt="signature">
                            </div>
                        </div>
                        <!-- .row -->
                    </section>
                    <!-- .section -->

                    <section class="section padd-box">
                        <div class="row">
                            <div class="col-sm-6 clear-mrg">
                                <h2 class="title-thin text-muted">información personal</h2>

                                <dl class="dl-horizontal clear-mrg">
                                    <dt class="text-upper">Nombre</dt>
                                    <dd>Ezequiel Paolillo</dd>

                                    <dt class="text-upper">Nacimiento</dt>
                                    <dd>09 julio 1989</dd>

                                    <dt class="text-upper">DNI</dt>
                                    <dd>34555306</dd>

                                    <dt class="text-upper">Domicilio</dt>
                                    <dd>Jean Jaures 55,
                                        CABA. Argentina.
                                    </dd>

                                    <dt class="text-upper">e-mail</dt>
                                    <dd><a href="mailto:paolilloe@gmail.com">paolilloe@gmail.com</a></dd>

                                    <dt class="text-upper">Teléfono</dt>
                                    <dd>+54 11 68894106</dd>

                                    <dt class="text-upper">freelance</dt>
                                    <dd>Disponible</dd>
                                </dl>
                            </div>
                            <!-- .col-sm-6 -->

                            <div class="col-sm-6 clear-mrg">
                                <h2 class="title-thin text-muted">idiomas</h2>

                                <div class="progress-bullets crt-animated" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="10">
                                    <strong class="progress-title">Español</strong>
                        <span class="progress-bar">
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                        </span>
                                    <span class="progress-text text-muted">nativo</span>
                                </div>

                                <div class="progress-bullets crt-animated" role="progressbar" aria-valuenow="8" aria-valuemin="0" aria-valuemax="10">
                                    <strong class="progress-title">Ingles</strong>
                        <span class="progress-bar">
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet fill"></span>
                            <span class="bullet"></span>
                            <span class="bullet"></span>
                            <span class="bullet"></span>
                            <span class="bullet"></span>
                        </span>
                                    <span class="progress-text text-muted">intermedio</span>
                                </div>

                            </div>
                            <!-- .col-sm-6 -->
                        </div>
                        <!-- .row -->
                    </section>
                    <!-- .section -->

                </div>
                <!-- .crt-paper-cont -->
            </div>
            <!-- .crt-paper -->
        </div>
        <!-- .crt-paper-layers -->

        <div id="experience" class="crt-paper-layers crt-animated">
            <div class="crt-paper clearfix">
                <div class="crt-paper-cont paper-padd clear-mrg">

                    <section class="section padd-box">
                        <h2 class="title-lg text-upper">experiencia laboral</h2>
                        <div class="education">
                            <!-- .education-box -->

                            <div class="education-box">
                                <time class="education-date" datetime="2014-01T2016-03">
                                    <span>Mar <strong class="text-upper">2016</strong> - <strong>Presente</strong></span>
                                </time>
                                <h3>Analista / PM / Desarrollador Full Stack</h3>
                                <div class="education-logo">
                                    <!--<img src="./Certy_files/logo-themeforest.png" alt="">-->
                                </div>
                                <span class="education-company">Gobierno de la Ciudad de Buenos Aires</span>
                                <p>
                                    Dentro del GCBA impulsé muchos proyectos relacionados a mejorar el proceso de comunicación entre empleados,
                                    implementación de Microsoft Dynamics CRM, mejora de procesos gracias al desarrollo e implementación de un sistema de gestión. 
                                </p>
                            </div>
                            
                            <!-- .education-box -->
                            <div class="education-box">
                                <time class="education-date" datetime="2014-01T2016-03">
                                    <span>Feb <strong class="text-upper">2015</strong> - <strong>Presente</strong></span>
                                </time>
                                <h3>Consultoría Tecnológica</h3>
                                <div class="education-logo">
                                    <!--<img src="./Certy_files/logo-themeforest.png" alt="">-->
                                </div>
                                <span class="education-company">Consilia</span>
                                <p>
                                    A principios de 2015 fundé Consilia, un
                                    emprendimiento unipersonal que permitió
                                    llegar a importantes clientes y dar soluciones
                                    tecnológicas específicas.
                                </p>
                            </div>
                            <!-- .education-box -->

                            <div class="education-box">
                                <time class="education-date" datetime="2010-01T2019-03">
                                    <span>Ago <strong class="text-upper">2010</strong> - <strong>Presente</strong></span>
                                </time>
                                <h3>Full Stack Developer</h3>
                                <div class="education-logo">
                                    <!--<img src="./Certy_files/logo-audio-jungle.png" alt="">-->
                                </div>
                                <span class="education-company">Comisión Nacional de Energía Atómica</span>
                                <p>Desde 2010 llevo adelante el sector tecnológico de la subgerencia Capital Intelectual, perteneciente a la Gerencia de Área Académica dando soporte a las nuevas necesidades informáticas que tiene el sector y gerencias relacionadas.
                                El foco esta en la mejora de procesos documentales, sistemas de gestión relacionados a recursos humanos y al área académica, transformación digital, migrando procesos y trámites del papel hacia lo digital, análisis de datos, hacer cumplir políticas de seguridad informática. 
                                </p>
                            </div>

                        </div>
                        <!-- .education -->
                    </section>
                    <!-- .section -->

                </div>
                <!-- .crt-paper-cont -->
            </div>
            <!-- .crt-paper -->
        </div>
        <!-- .crt-paper-layers -->

        <div id="portfolio" class="crt-paper-layers crt-animated">
            <div class="crt-paper clearfix">
                <div class="crt-paper-cont paper-padd clear-mrg">

                    <section class="section padd-box">
                    <h2 class="title-lg text-upper padd-box">proyectos destacados</h2>

                    <div class="pf-wrap">

                    <div class="pf-grid" style="position: relative; height: 1000px;">
                    <div class="pf-grid-sizer"></div><!-- used for sizing -->


                    

                    <div class="pf-grid-item photography">
                        <a class="pf-project" href="#pf-popup-1">
                            <figure class="pf-figure">
                                <img src="./img/badesdeadentro.jpg" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">BA Desde Adentro</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Portal empleado, es el punto de encuentro de todos los empleados del GCBA, y reune a un público recurrente de aproximadamente 150.000 usuarios.</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">ver más</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="pf-popup-1" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/badesdeadentro/1.png" alt="">
                                    </div>

                                    <div class="pf-popup-embed">
                                        <img src="./img/badesdeadentro/2.png" alt="">
                                    </div>

                                    <div class="pf-popup-embed">
                                        <img src="./img/badesdeadentro/3.png" alt="">
                                    </div>
                                </div>
                                <ul>
                                    <li>Incremento 20% visitas a contenido editorial</li>
                                    <li>+ 70k usuarios se sumaron como visitas, 80% Público total empleados</li>
                                    <li>Segmentación de contenido según perfil</li>
                                    <li>Validación WCAG de Accesibilidad</li>
                                    <li>Sistema de tracking, de comportamiento</li>
                                    <li>Alertas via Popups segmentados al empleado</li>
                                    <li>Notificaciones Webpush para eventos importantes</li>
                                    <li>Calendario de eventos</li>
                                    <li>Webapps (Prode, Galerias de fotos con trackeo de rostros, etc)</li>
                                </ul>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">BA Desde Adentro</h2>

                                    <div class="text-muted"><strong>diseño / desarrollo / comunidad</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha</dt>
                                        <dd>11 Mar 2018</dd>
                                    </dl>

                                    <p>Tomamos junto con el equipo de BA Desde Adentro modificar el antiguo portal de empleado.
                                    La principal restricción fue que no contábamos con acceso al código fuente del portal, dado
                                    que se trataba de un Drupal legacy. La solución fue el desarrollo de un enfoque client-side, 
                                    se importaron todos los datos segmentados por usuario via JSON a través de webservices.
                                    El resultado fue que conseguimos segmentar el contenido por usuario, comunidades prácticas,
                                    repartición, etc.</p>
                                    <p>El desarrollo continúa día a día realizando mejoras continuas, test AB, mediciones sobre PKI,
                                    y monitoreo del comportamiento del público mediante mapa de calor e instalacion de webapps custom.</p>
                                    <p>Todas estas innovaciones que sumamos tuvieron un impacto directo, conseguimos aumentar
                                    un 20% las visitas al contenido deseado.</p>
                                        
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->



                    <div class="pf-grid-item design">
                        <a class="pf-project" href="#degac">
                            <figure class="pf-figure">
                                <img src="./img/degac.png" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Sistema DEGAC</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>DEGAC S.A. es una empresa de transporte fundada en 1995, sirve a todo el proceso de retail de cadenas como COTO, Frávega y Walmart.</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">ver más</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="degac" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/degac1.png" alt="">
                                    </div>
                                    <div class="pf-popup-embed">
                                        <img src="./img/degac2.png" alt="">
                                    </div>
                                    <div class="pf-popup-embed">
                                        <img src="./img/degac3.png" alt="">
                                    </div>
                                </div>
                                <p>
                                La necesidad del cliente era contar con un sistema único donde puedan controlar todos los aspectos operativos de la empresa, desde la asignación de móviles a pedidos de clientes, gestión de productos, gestión de inventarios, moviles y choféres.
                                </p>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Sistema DEGAC SA.</h2>

                                    <div class="text-muted"><strong>análisis funcional / desarrollo</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>03 Mar 2015</dd>

                                        <dt>Cliente:</dt>
                                        <dd>DEGAC SA</dd>
                                    </dl>

                                    <p>

                                        Durante el análisis logré detectar necesidades a cubrir que no estaban manifiestas de antemano, por ejemplo, el sistema cuenta también con cuadros de balances que permiten determinar en tiempo real la rentabilidad económica de cada móvil, tomando como parámetros de entrada los insumos consumidos, los viajes realizados y el kilometraje total.
                                        <br>
                                        <br>
                                        También se encontró la necesidad de almacenar correctamente la información relativa a cada chofér, su licencia, documentacion, registro, antiguedad.
                                        <br>
                                        <br>
                                        Es un sistema en constante cambio, desarrollado utilizando metodologias agiles y frameworks con estándares world class que nos permiten modificar lógicas en producción
                                    </p>
                                </div><!-- .pf-popup-info -->

                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->

                    <div class="pf-grid-item photography tracking-intercom">
                        <a class="pf-project" href="#gc">
                            <figure class="pf-figure">
                                <img src="./img/gc.png" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Gestión del Conocimiento</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Sistema de gestión del conocimiento - GCBA</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">View More</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="gc" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/gc1.jpg" alt="">
                                    </div>
                                </div>
                                <p>
                                Utilizamos como plataforma base Outline, que nos provee de una base de conocimiento utilizando nodeJS.
                                </p>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Gestión del Conocimiento</h2>

                                    <div class="text-muted"><strong>analisis / desarrollo / estudio</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>30 Nov 2018</dd>

                                        <dt>Cliente:</dt>
                                        <dd>GCBA</dd>
                                    </dl>

                                    <p>Este proyecto actualmente en desarrollo viene a formar parte de una solución integral que cubre aspectos tecnológicos, de procesos y de cambio cultural. Mi rol de consultor en tecnología fue la de formular un analisis de requerimientos para una plataforma que venga a dar apoyo a los colaboradores del grupo de gestión de conocimiento.
                                    <br>
                                    <br>
                                    Se realizó un profundo análisis de que tecnología utilizar y que características debe tener la solución final. Que en virtud de ser interdiciplinaria cubre aspectos desde gamificación para el seguimiento de las unidades de aprendizaje, hasta procesamiento del lenguaje natural para el buscador.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->

                    <div class="pf-grid-item photography">
                        <a class="pf-project" href="#dia">
                            <figure class="pf-figure">
                                <img src="./img/dia.jpg" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Transformación Digital DIA</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Digitalización del proceso de fidelización de clientes de DIA, pasando de formularios papel a una experiencia 100% digital.</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">View More</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="dia" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/dia.jpg" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Transformación digital Dia</h2>

                                    <div class="text-muted"><strong>diseño / desarrollo</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>11 Ene 2015</dd>

                                        <dt>Cliente:</dt>
                                        <dd>Dia</dd>
                                    </dl>

                                    <p>El programa de fidelización de Supermercado Dia consitía en un formulario de papel que cada cliente debia completar con sus datos en cada sucursal, posteriormente estas altas las cargaban en las oficinas a mano el sector de data entry. Posteriormente se cruzaban las altas con las existentes en la base de datos en españa.
                                    <br>
                                    El proceso descripto no estaba excento de cientos de errores al mes, ya que la carga manual introducia errores que desbalanceaban las bases de datos. Dando lugar a clientes repetidos, pésima calidad de datos que imposibilitaba cualquier visión a futuro en BI, y encima una carga de trabajo inmensa.
                                    <br>
                                    <br>
                                    La solución fue desarrollar un tótem digital, los clientes cargan sus datos via el tótem, e inmediatamente se imprime un ticket, con este ticket retiran la tarjeta de cliente.
                                    Todo este proceso no toma mas de 5 minutos.
                                    Una vez al dia se envia a españa de forma automatizada las novedades, ahora pudiendo informar modificaciones, evitando datos duplicados y ahorrando cientas de horas hombre.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->

                    <div class="pf-grid-item design">
                        <a class="pf-project" href="#mymtg">
                            <figure class="pf-figure">
                                <img src="./img/mymtg.gif" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">myMTG</h2>

                                        <div class="pf-text">
                                            <p>Geolocalización indoor, Barcelona. España.</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">ver más</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="mymtg" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/mymtg1.png" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">myMTG</h2>

                                    <div class="text-muted"><strong>desarrollo / publicación</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>01 May 2015</dd>

                                        <dt>Cliente:</dt>
                                        <dd>MTG Net SA</dd>
                                    </dl>

                                    <p>Desarrollamos con el cliente una aplicación mobile, que permite geolocalizar indoor a un potencial cliente. Utilizamos una tecnología llamada beacons, que nos provee la proximidad del posible cliente y la posibilidad de enviarle una alerta con promociones de acuerdo a su ubicación.
                                    <br>
                                    <br>
                                    El despliegue fue completo, tanto para la app store de Google como la de Apple, siendo esta última un verdadero reto por las altas exigencias de Apple.
                                    <br>
                                    <br>
                                    El circuito de promoción funciona en el metro de Barcelona, España.
                                    Pero hoy en dia desarrollé y probé tecnologias de geolocalización indoor mucho mas económicas y sin hardware complementario.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->

                    <div class="pf-grid-item photography">
                        <a class="pf-project" href="#launchsmith">
                            <figure class="pf-figure">
                                <img src="./img/launchsmith.png" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Launchsmith</h2>

                                        <div class="pf-text">
                                            <p>Self-assesment test + site, Industria farmacéutica, EEUU.</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">ver más</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="launchsmith" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/launchsmith1.png" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Launchsmith</h2>

                                    <div class="text-muted"><strong>diseño / desarrollo</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>01 Ago 2016</dd>

                                        <dt>Sitio:</dt>
                                        <dd><a href="http://www.launchsmith.com">www.launchsmith.com</a></dd>

                                        <dt>Cliente:</dt>
                                        <dd>Launchsmith</dd>
                                    </dl>

                                    <p>La compañía estadounidense requería un portal de captura de leads que tenga alguna particularidad diferenciadora.
                                    <br>
                                    <br>
                                    Trabajé en este proyecto con un estudio llamado Yapa, y elaboramos un portal que cuenta con un test llamado "self-assessment" orientado a responsables de compañias farmacéuticas que deseen añadir un engage a la comercialización de sus productos.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->
                    <div class="pf-grid-item design">
                        <a class="pf-project" href="#cloud">
                            <figure class="pf-figure">
                                <img src="./img/cloud.jpg" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Migración Cloud GCBA</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Migración a la nube de aplicaciones y sistemas dentro del GCBA</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">ver más</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="cloud" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/cloud.jpg" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">cloud web apps</h2>

                                    <div class="text-muted"><strong>infraestructura cloud</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>03 Mar 2018</dd>

                                        <dt>Cliente:</dt>
                                        <dd>GCBA</dd>
                                    </dl>

                                    <p>En esta oportunidad colaboré con la migración y desarrollo de aplicaciones web embebidas hosteadas en la nube. Esta capacidad nos permitió contar con recursos para los empleados de gobierno que de otra forma hubiera sido imposible, al mismo tiempo que maximizamos la performance de los aplicativos que utilizaban estas webapps.</p>
                                    <br>
                                    <br>
                                    El resultado fue medido a través de multiples encuestas de satisfacción, encuestas, que a su vez provenian de un servicio cloud. El resultado fue muy positivo, y tuvimos un muy alto nivel de participación.
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->
                    <div class="pf-grid-item photography">
                        <a class="pf-project" href="#sipi">
                            <figure class="pf-figure">
                                <img src="./img/sipi.png" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">SIPI - ERP</h2>

                                        <div class="pf-text">
                                            <p>Desarrollo e implementación del software de gestión de proyectos de inversion - CNEA.</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">ver más</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="sipi" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/sipi.png" alt="">
                                        (Imagen ilustrativa)
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">SIPI ERP</h2>

                                    <div class="text-muted"><strong>diseño / desarrollo / analisis funcional</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>01 Ago 2016</dd>

                                        <dt>Cliente:</dt>
                                        <dd>GCBA | CNEA</dd>
                                    </dl>

                                    <p>SIPI es un ERP desarrollado integramente a partir de DOT Project, pero extendido a las necesidades de cada cliente. En el caso de la Comisión Nacional de Energía Atómica, el desarrollo del mismo fue orientado a proyectos de inversión, donde es posible apreciar el avance de cada uno, subir fotografías de predios y confeccionar reportes ejecutivos.
                                    <br>
                                    <br>
                                    En GCBA en cambio, lo utilizamos para llevar adelante los procesos de compras, la ejecución de los presupuestos y la planificación a futuro.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div>
                    

                    <div class="pf-grid-item design" >
                        <a class="pf-project" href="#alcalido">
                            <figure class="pf-figure">
                                <img src="./img/ml.jpg" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Alcálido - CNEA</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>IA + Procesamiento de lenguaje natural. Sistema de documentación con búsqueda de archivos utilizando Inteligencia Artificial</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">ver más</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="alcalido" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/ml.jpg" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Alcálido</h2>

                                    <div class="text-muted"><strong>diseño / desarrollo / estudio</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>01 Sep 2018</dd>

                                        <dt>Cliente:</dt>
                                        <dd>Comisión Nacional de Energía Atómica</dd>
                                    </dl>

                                    <p>Este proyecto permite buscar entre lotes enormes de documentación no estructurada (generalmente PDFs o DOCs) de disposiciones, expedientes, y otros tipos de contenidos utilizando procesamiento de lenguaje natural.
                                    Desarrollamos un buscador que utiliza una SVM para caracterizar cada documento mediante una funcion batch de indexación. Permitiendo luego una busqueda muchísimo mas exacta que por palabras claves. Es posible incluso, realizarle preguntas.
                                    Las SVM son un tipo de machine learning, aún no muy extensamente utilizadas.</p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->

                    <div class="pf-grid-item photography" >
                        <a class="pf-project" href="#mvl">
                            <figure class="pf-figure">
                                <img src="./img/mvl.jpg" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">MVL - Información</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Portal de accesos estadísticos, información fiscal. Municipalidad de Vicente Lopez.</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">View More</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="mvl" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/mvl1.png" alt="">
                                    </div>
                                    <div class="pf-popup-embed">
                                        <img src="./img/mvl2.png" alt="">
                                    </div>
                                    <div class="pf-popup-embed">
                                        <img src="./img/mvl3.png" alt="">
                                    </div>
                                    <div class="pf-popup-embed">
                                        <img src="./img/mvl4.png" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Información Fiscal MVL</h2>

                                    <div class="text-muted"><strong>diseño / desarrollo</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>11 Ene 2015</dd>

                                        <dt>Site link:</dt>
                                        <dd><a href="http://www.vicentelopez.gov.ar/informacion_fiscal/">www.vicentelopez.gov.ar/informacion_fiscal</a></dd>

                                        <dt>Cliente:</dt>
                                        <dd>Municipalidad de Vicente López</dd>
                                    </dl>

                                    <p>Realizamos el portal para la sección de Información Fiscal en la Municipalidad de Vicente López, el requerimiento original consitia en un portal autoadministrable, pero querían evitar la utilización de un CMS convencional estilo Wordpress o Drupal. Desarrollamos uno 100% visual, que permite que cualquier colaborador de la municipalidad sin conocimientos en una herramienta particular, pueda subir información. El resultado redunda en una mejoría notable en los procesos de comunicación.
                                    <br>
                                    <br>
                                    Tiempo después me pidieron medir este impacto, y lo realizamos mediante una encuesta de satisfacción cuyos resultados fueron muy positivos.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->
                    <div class="pf-grid-item photography" >
                        <a class="pf-project" href="#seg">
                            <figure class="pf-figure">
                                <img src="./img/wpscan.png" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Ciberseguridad - Plan de seguridad Informática - GCBA CNEA</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Auditoría de seguridad informática, pentesting, consideraciónes de seguridad informática</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">View More</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="seg" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/wpscan.png" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Ciberseguridad</h2>

                                    <div class="text-muted"><strong>design / development</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>01 Mar 2018</dd>

                                        <dt>Cliente:</dt>
                                        <dd>GCBA</dd>
                                    </dl>

                                    <p>Una de mis funciones propuestas como consultor fue de asesorar en materia de seguridad informática. Para ello realizamos testings sobre los aplicativos existentes, estudio de las políticas actuales, usos y costumbres en la operatoria diaria, y continuamente realizamos reportes para la gerencia en pos de mejorar los aspectos que junto a los colaboradores identificamos como débiles.
                                    <br>
                                    <br>
                                    Hoy en día la seguridad informática debe tenerse muy en cuenta, y ser un pilar fundamental que comienza desde el dia 0 de cualquier proyecto, aún si no es directamente tecnológico.
                                    No atender debidamente todas las cuestiones inherentes a la seguridad informática da prueba de una culpable indiferencia hacia la disponibilidad, confidencialidad e integridad de los activos de información, y ello no debe ocurrir.
                                    <br>
                                    En función de las exigencias del estado del arte actual me formo continua e intensamente en esta área del conocimiento que considero personalmente estratégica para el futuro, tanto desde el punto de vista legal como técnico.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->
                    
                    <div class="pf-grid-item photography">
                        <a class="pf-project" href="#hadoop">
                            <figure class="pf-figure">
                                <img src="./img/hadoop.png" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Big data</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Hadoop Ecosistema - GCBA.</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">View More</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="hadoop" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/hadoop.png" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Implementación Hadoop</h2>

                                    <div class="text-muted"><strong>big data</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>01 Mar 2019</dd>

                                        <dt>Cliente:</dt>
                                        <dd>GCBA</dd>
                                    </dl>

                                    <p>Actualmente la información recopilada sobre accesos a los sistemas es tratada con bases de datos relacionales.
                                    Si bien la preparación de reportes no es demasiado complicada en términos de tiempo, implementé un muy pequeño cluster de varios nodos con tecnología Hadoop, para generar reporting intensivo mas rápidamente.
                                    <br>
                                    <br>
                                    La mejora resultó en información inmediata para la secretaría a fín de exponer datos en reportes muy rápidamente.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->


                    <div class="pf-grid-item photography tracking-intercom">
                        <a class="pf-project" href="#tracking">
                            <figure class="pf-figure">
                                <img src="./img/bada_ing.png" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Tracking Managment System</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Panel de control de trackeo de comportamiento de usuarios</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">View More</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="tracking" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                        <img src="./img/tracking1.png" alt="">
                                    </div>
                                    <div class="pf-popup-embed">
                                        <img src="./img/tracking2.png" alt="">
                                    </div>
                                </div>
                                    <p>
                                    Realmente no existe el concepto de Tracking Managment System, pero decidí llamarlo asi en función a todas las herramientas que provee este desarrollo.
                                    </p>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Tracking Managment System</h2>

                                    <div class="text-muted"><strong>diseño / desarrollo / analisis funcional</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>01 Abr 2018</dd>

                                        <dt>Cliente:</dt>
                                        <dd>GCBA | SCCYFP</dd>
                                    </dl>

                                    <p>Este sistema se encarga de mostrar en reportes la utilización de los canales de consulta, asi como de atender solicitudes online, evitando tener que utilizar el teléfono ya que éste no es un buen canal para trackear un caso de un usuario con algún problema particular.
                                    <br>
                                    Contiene un chat interno que se utiliza en todos los sitios que se desea, monitoreo de cantidad de visitas, por tema facilitando segmentar el contenido en función de los intereses de las visitas.
                                    <br>
                                    Cuenta también con un módulo de suscripciones push, lo que nos permite enviar alertas a celular o PC utilizando la tecnología de notificaciones Push.
                                    <br>
                                    <br>
                                    Permite generar galerias de imagenes que puedan embeberse en sitios webs, y también crear notificaciones popups para sitios. 

                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->



                    <div class="pf-grid-item photography tracking-intercom">
                        <a class="pf-project" href="#evaluacion">
                            <figure class="pf-figure">
                                <img src="./img/evaluacion.jpeg" alt="">
                            </figure>

                            <div class="pf-caption text-center">
                                <div class="valign-table">
                                    <div class="valign-cell">
                                        <h2 class="pf-title text-upper">Evaluación de Desempeño</h2>

                                        <div class="pf-text clear-mrg">
                                            <p>Sistema de evaluación de desempeño - GCBA</p>
                                        </div>

                                        <button class="pf-btn btn btn-primary">View More</button>
                                    </div>
                                </div>
                            </div>
                        </a><!-- .pf-project -->

                        <div id="evaluacion" class="pf-popup clearfix">
                            <div class="pf-popup-col1">
                                <div class="pf-popup-media cr-slider" data-init="none">
                                    <div class="pf-popup-embed">
                                    <img src="./img/evaluacion.jpeg" alt="">
                                    </div>
                                </div>
                            </div><!-- .pf-popup-col1 -->

                            <div class="pf-popup-col2">
                                <div class="pf-popup-info">
                                    <h2 class="pf-popup-title text-upper">Evaluación de Desempeño</h2>

                                    <div class="text-muted"><strong>diseño / análisis funcional / desarrollo</strong></div>

                                    <dl class="dl-horizontal">
                                        <dt>Fecha:</dt>
                                        <dd>01 Oct 2018</dd>

                                        <dt>Cliente:</dt>
                                        <dd>GCBA</dd>
                                    </dl>

                                    <p>A fin de realizar algunas de las evaluaciones de desempeño en el sector, realizamos este desarrollo que consiste en una encuesta, a completar por cada responsable, que entrega al finalizar un resultado en PDF.
                                    <br>
                                    <br>
                                    El objetivo era conseguir mejorar los tiempos de respuesta y de adquisición de las encuestas, para ello generamos este sistema que en tiempo real nos entrega los resultados utilizando reportes de BI.
                                    <br>
                                    <br>
                                    El resultado final es computado en tiempo real y entregado a la persona que realiza la encuesta.
                                    </p>
                                </div><!-- .pf-popup-info -->

                                
                            </div><!-- .pf-popup-col2 -->
                        </div><!-- .pf-popup -->
                    </div><!-- .pf-grid-item -->
                    
                    
                    
                    </div><!-- .pf-grid -->
                    </div><!-- .pf-wrap -->
                    </section>
                    <!-- .section -->

                </div>
                <!-- .crt-paper-cont -->
            </div>
            <!-- .crt-paper -->
        </div>
        <!-- .crt-paper-layers -->


        <div id="references" class="crt-paper-layers crt-animated">
            <div class="crt-paper clearfix">
                <div class="crt-paper-cont paper-padd clear-mrg">


                    <section class="section padd-box">
                        <h2 class="title-lg text-upper">formación académica</h2>
                        <div class="education">
                            <!-- .education-box -->

                            <div class="education-box">
                                <time class="education-date" datetime="2014-01T2016-03">
                                    <span>Mar <strong class="text-upper">2019</strong></span>
                                </time>
                                <h3>Ingeniería en Sistemas de Información</h3>
                                <span class="education-company">Universidad Tecnológica Nacional Facultad Regional Buenos Aires</span>
                                <p>
                                    Cursando últimas 3 materias (Electivas)<br>
                                    Orientación en innovación, nuevos negocios y seguridad informática
                                </p>
                            </div>

                            <div class="education-box">
                                <time class="education-date" datetime="2014-01T2016-03">
                                    <span>Ago <strong class="text-upper">2018</strong></span>
                                </time>
                                <h3>Diplomatura en Big Data</h3>
                                <span class="education-company">Universidad Tecnológica Nacional Facultad Regional Buenos Aires</span>
                                <p>
                                    Desarrollo y planificación de arquitecturas distribuidas y concurrentes. Ciencias de datos, fundamentos teóricos y prácticos.
                                    Entorno Hadoop, Bases de datos noSQL, Analisis y scheduling de procesos de entrada salida y CPU bounded.
                                </p>
                            </div>

                            <div class="education-box">
                                <time class="education-date" datetime="2014-01T2016-03">
                                    <span>Ago <strong class="text-upper">2014</strong></span>
                                </time>
                                <h3>School of Nuclear Knowledge Managment</h3>
                                <span class="education-company">International Centre for Theorical Physics, Italia.</span>
                                <p>
                                    Estudios en matería de gestión del conocimiento con orientación en la industria nuclear.
                                </p>
                            </div>

                            <div class="education-box">
                                <time class="education-date" datetime="2014-01T2016-03">
                                    <span>Ene <strong class="text-upper">2008</strong></span>
                                </time>
                                <h3>Técnico Electrónico</h3>
                                <span class="education-company">ET N° 28 República Francesa</span>
                                <p>
                                Participación en concursos de roboótica.
                                </p>
                            </div>
                            

                        </div>
                        <!-- .education -->
                    </section>


                </div>
                <!-- .crt-paper-cont -->
            </div>
            <!-- .crt-paper -->
        </div>
        <!-- .crt-paper-layers -->



        <div id="references" class="crt-paper-layers crt-animated">
            <div class="crt-paper clearfix">
                <div class="crt-paper-cont paper-padd clear-mrg">

                    <section class="section padd-box">
                            <h2 class="title-lg text-upper">Referencias</h2>

                            <div class="padd-box-sm clear-mrg">
                                <div class="ref-box brd-btm hreview">
                                <div class="ref-avatar">
                                        <img alt="" src="./Certy_files/avatar-58x58-default.png" srcset="assets/images/uploads/avatar/avatar-116x116-default-2x.jpg 2x" class="avatar avatar-54 photo" height="54" width="54">
                                    </div>
                                    <div class="ref-info">
                                        <div class="ref-author">
                                            <strong>Lic. Maria Eugenia Botteri Domecq</strong>
                                            <span>Dirección General Cultura del Servicio Público <br> Secretaría de Cultura Ciudadana y Función Pública <br> GCBA</span>
                                        </div>
                                        <small>mbotteridomecq@buenosaires.gob.ar</small>
                                    </div>
                                </div><!-- .ref-box -->

                                <div class="ref-box brd-btm hreview">
                                <div class="ref-avatar">
                                        <img alt="" src="./Certy_files/avatar-58x58-default.png" srcset="assets/images/uploads/avatar/avatar-116x116-default-2x.jpg 2x" class="avatar avatar-54 photo" height="54" width="54">
                                    </div>
                                    <div class="ref-info">
                                        <div class="ref-author">
                                            <strong>Lic. Alejandra Teresa Chavez Flores</strong>
                                            <span>Subgerencia Capital Intelectual <br> Gerencia de Área Académica <br> Comisión Nacional de Energía Atómica</span>
                                        </div>
                                        <small>chavez@cnea.gov.ar</small>
                                    </div>
                                </div><!-- .ref-box -->

                                <div class="ref-box brd-btm hreview">
                                    <div class="ref-avatar">
                                        <img alt="" src="./Certy_files/avatar-58x58-default.png" srcset="assets/images/uploads/avatar/avatar-116x116-default-2x.jpg 2x" class="avatar avatar-54 photo" height="54" width="54">
                                    </div>

                                    <div class="ref-info">
                                        <div class="ref-author">
                                            <strong>Ing. Juan Marcos Alonso Abella</strong>
                                            <span>Jefe de Planificación Operativa <br> Vialidad Nacional <br> Ministerio de Transporte </span>
                                        </div>
                                        <small>jalonsoabella@gmail.com</small>
                                    </div>
                                </div><!-- .ref-box -->

                            </div><!-- .padd-box-sm -->
                    </section>
                    <!-- .section -->

                </div>
                <!-- .crt-paper-cont -->
            </div>
            <!-- .crt-paper -->
        </div>
        <!-- .crt-paper-layers -->
        

                    

    </div>
    <!-- .crt-container -->

    <div id="crt-sidebar">
    <button id="crt-sidebar-close" class="btn btn-icon btn-light btn-shade">
        <span class="crt-icon crt-icon-close"></span>
    </button>

    <div id="crt-sidebar-inner" class="mCustomScrollbar _mCS_1 mCS-autoHide" style="position: relative; overflow: visible;"><div id="mCSB_1" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" style="max-height: none;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
                <nav id="crt-main-nav-sm" class="visible-xs text-center">
            
    <ul class="clear-list">
        <li><a href="https://certy.px-lab.com/html/demo1/index.php">home</a></li>
        <li><a href="https://certy.px-lab.com/html/demo1/portfolio.php">portfolio</a>
        </li><li class="has-sub-menu"><a href="https://certy.px-lab.com/html/demo1/#">pages</a>
            <ul class="sub-menu">
                <li><a href="https://certy.px-lab.com/html/demo1/typography.php">typography</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/components.php">components</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/search.php">search</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/404.php">404</a></li>
            </ul>
        </li>
        <li class="has-sub-menu"><a href="https://certy.px-lab.com/html/demo1/category.php">blog</a>
            <ul class="sub-menu">
                <li><a href="https://certy.px-lab.com/html/demo1/single.php">single</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/single-image.php">single image</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/single-slider.php">single slider</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/single-youtube.php">single youtube</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/single-vimeo.php">single vimeo</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/single-dailymotion.php">single dailymotion</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/single-soundcloud.php">single soundcloud</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/single-video.php">single video</a></li>
                <li><a href="https://certy.px-lab.com/html/demo1/single-audio.php">single audio</a></li>
            </ul>
        </li>
        <li><a href="https://certy.px-lab.com/html/demo1/contact.php">contact</a></li>
    </ul>        </nav>
        
        
<div class="crt-card bg-primary text-center">
    <div class="crt-card-avatar">
        <img class="avatar avatar-195 mCS_img_loaded" src="./Certy_files/avatar-195x195.png" srcset="assets/images/uploads/avatar/avatar-390x390-2x.png 2x" width="195" height="195" alt="">
    </div>
    <div class="crt-card-info">
        <h2 class="text-upper">Ezequiel Paolillo</h2>

        <p class="text-muted">Consultor en Teconología</p>
        <ul class="crt-social clear-list">
            <li><a><span class="crt-icon crt-icon-facebook"></span></a></li>
            <li><a><span class="crt-icon crt-icon-twitter"></span></a></li>
            <li><a><span class="crt-icon crt-icon-google-plus"></span></a></li>
            <li><a><span class="crt-icon crt-icon-instagram"></span></a></li>
            <li><a><span class="crt-icon crt-icon-pinterest"></span></a></li>
        </ul>
    </div>
</div>
        <aside class="widget-area">
            <section class="widget widget_search">
                <form role="search" method="get" class="search-form" action="https://certy.px-lab.com/html/demo1/#">
                    <label>
                        <span class="screen-reader-text">Search for:</span>
                        <input type="search" class="search-field" placeholder="Search" value="" name="s">
                    </label>
                    <button type="submit" class="search-submit">
                        <span class="screen-reader-text">Search</span>
                        <span class="crt-icon crt-icon-search"></span>
                    </button>
                </form>
            </section>

            <section class="widget widget_posts_entries">
                <h2 class="widget-title">popular posts</h2>
                <ul>
                    <li>
                        <a class="post-image" href="https://certy.px-lab.com/html/demo1/">
                            <img src="./Certy_files/img-70x70-01.png" alt="" class="mCS_img_loaded">
                        </a>
                        <div class="post-content">
                            <h3>
                                <a href="https://certy.px-lab.com/html/demo1/">contextual advertising</a>
                            </h3>
                        </div>
                        <div class="post-category-comment">
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-category">Work</a>
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-comments">256 comments</a>
                        </div>
                    </li>

                    <li>
                        <a class="post-image" href="https://certy.px-lab.com/html/demo1/">
                            <img src="./Certy_files/img-70x70-02.jpg" alt="" class="mCS_img_loaded">
                        </a>
                        <div class="post-content">
                            <h3>
                                <a href="https://certy.px-lab.com/html/demo1/">grilling tips for the dog days of summer</a>
                            </h3>
                        </div>
                        <div class="post-category-comment">
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-category">Work</a>
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-comments">256 comments</a>
                        </div>
                    </li>

                    <li>
                        <a class="post-image" href="https://certy.px-lab.com/html/demo1/">
                            <img src="./Certy_files/img-70x70-03.png" alt="" class="mCS_img_loaded">
                        </a>
                        <div class="post-content">
                            <h3><a href="https://certy.px-lab.com/html/demo1/"></a>branding do you know who are</h3>
                        </div>
                        <div class="post-category-comment">
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-category">Work</a>
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-comments">256 comments</a>
                        </div>
                    </li>
                </ul>
            </section>

            <section id="tag_cloud-2" class="widget widget_tag_cloud">
                <h2 class="widget-title">Tags</h2>
                <div class="tagcloud">
                    <a href="https://certy.px-lab.com/html/demo1/" class="tag-link-5 tag-link-position-1" title="1 topic" style="font-size: 1em;">Audios</a>
                    <a href="https://certy.px-lab.com/html/demo1/" class="tag-link-7 tag-link-position-2" title="1 topic" style="font-size: 1em;">Freelance</a></div>
            </section>

            <section id="recent-posts-3" class="widget widget_recent_entries">
                <h4 class="widget-title">Recent Posts</h4>
                <ul>
                    <li>
                        <a href="https://certy.px-lab.com/html/demo1/">Global Travel And Vacations  Luxury Travel On A Tight  Budget</a>
                        <div class="post-category-comment">
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-category">Photography</a>
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-comments">256 comments</a>
                        </div>
                    </li>
                    <li>
                        <a href="https://certy.px-lab.com/html/demo1/">cooking for one</a>
                        <div class="post-category-comment">
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-category">Work</a>
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-comments">256 comments</a>
                        </div>
                    </li>
                    <li>
                        <a href="https://certy.px-lab.com/html/demo1/">An Ugly Myspace Profile Will  Sure Ruin Your Reputation</a>
                        <div class="post-category-comment">
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-category">Photography</a>
                            <a href="https://certy.px-lab.com/html/demo1/" class="post-comments">256 comments</a>
                        </div>
                    </li>
                </ul>
            </section>

            <section class="widget widget_categories">
                <h4 class="widget-title">post categories</h4>
                <ul>
                    <li class="cat-item"><a href="https://certy.px-lab.com/html/demo1/">Audios</a>5</li>
                    <li class="cat-item"><a href="https://certy.px-lab.com/html/demo1/">Daili Inspiration</a>2</li>
                    <li class="cat-item"><a href="https://certy.px-lab.com/html/demo1/">Freelance</a>27</li>
                    <li class="cat-item"><a href="https://certy.px-lab.com/html/demo1/">Links</a>5</li>
                    <li class="cat-item"><a href="https://certy.px-lab.com/html/demo1/">Mobile</a>2</li>
                    <li class="cat-item"><a href="https://certy.px-lab.com/html/demo1/">Phography</a>27</li>
                </ul>
            </section>
        </aside>

    </div></div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-minimal-dark mCSB_scrollTools_vertical" style="display: block;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 50px; display: block; height: 355px; max-height: 826px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 50px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div><!-- #crt-sidebar-inner -->
</div><!-- #crt-sidebar -->
    <footer id="crt-footer" class="crt-container-lg">
        <div class="crt-container">
            <div class="crt-container-sm clear-mrg text-center">
                <p></p>
            </div>
        </div>
        <!-- .crt-container -->
    </footer>
    <!-- #crt-footer -->

        <svg id="crt-bg-shape-1" class="hidden-sm hidden-xs" height="519" width="758">
        <polygon class="pol" points="0,455,693,352,173,0,92,0,0,71"></polygon>
    </svg>

    <svg id="crt-bg-shape-2" class="hidden-sm hidden-xs" height="536" width="633">
        <polygon points="0,0,633,0,633,536"></polygon>
    </svg>
    </div><div id="_atssh" style="visibility: hidden; height: 1px; width: 1px; position: absolute; top: -9999px; z-index: 100000;"><iframe id="_atssh120" title="AddThis utility frame" src="./Certy_files/sh.e4e8af4de595fdb10ec1459d.html" style="height: 1px; width: 1px; position: absolute; top: 0px; z-index: 100000; border: 0px; left: 0px;"></iframe></div><style id="service-icons-0"></style>
<!-- .crt-wrapper -->

<!-- Scripts -->
<script type="text/javascript" src="./Certy_files/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.12.4.min.js"><\/script>')</script>
<script type="text/javascript" src="./Certy_files/js"></script>

<script type="text/javascript" src="./Certy_files/plugins.min.js"></script>
<script type="text/javascript" src="./Certy_files/theme.min.js"></script>

</body></html>
